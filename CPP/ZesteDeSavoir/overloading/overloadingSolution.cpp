#include <iostream>
#include <tuple>

struct Duree
{
    int secondes;
};

Duree creer_duree(int secondes, int minutes = 0, int heures = 0);
int en_secondes(Duree d);
std::tuple<int, int> en_minutes(Duree d);
std::tuple<int, int, int> en_heures(Duree d);

Duree operator+(Duree const& lhs, Duree const& rhs);
Duree operator-(Duree const& d);
Duree operator-(Duree const& lhs, Duree const& rhs);

bool operator==(Duree const& lhs, Duree const& rhs);
bool operator!=(Duree const& lhs, Duree const& rhs);
bool operator<(Duree const& lhs, Duree const& rhs);
bool operator>(Duree const& lhs, Duree const& rhs);
bool operator<=(Duree const& lhs, Duree const& rhs);
bool operator>=(Duree const& lhs, Duree const& rhs);

std::ostream& operator<<(std::ostream& os, Duree d);

int main()
{
    

    return 0;
}

Duree creer_duree(int secondes, int minutes, int heures)
{
    return Duree{secondes + 60 * minutes + 3600 * heures};
}

int en_secondes(Duree d)
{
    return d.secondes;
}

std::pair<int, int> div_eucl_ordinaire(int i, int div)
{
    if(i >= 0)
        return {i / div, i % div};
    
    return {i / div - 1, i % div + 60};
}

std::tuple<int, int> en_minutes(Duree d)
{
    return div_eucl_ordinaire(en_secondes(d), 60);
}

std::tuple<int, int, int> en_heures(Duree d)
{
    auto [minutes, secondes] = en_minutes(d);
    auto heures{0};
    std::tie(heures, minutes) = div_eucl_ordinaire(minutes, 60);
    return {heures, minutes, secondes};
}


Duree operator+(Duree const& lhs, Duree const& rhs)
{
    return Duree{lhs.secondes + rhs.secondes};
}

Duree operator-(Duree const& d)
{
    return Duree{-d.secondes};
}

Duree operator-(Duree const& lhs, Duree const& rhs)
{
    return lhs + (-rhs);
}


bool operator==(Duree const& lhs, Duree const& rhs)
{
    return lhs.secondes == rhs.secondes;
}

bool operator!=(Duree const& lhs, Duree const& rhs)
{
    return !(lhs == rhs);
}

bool operator<(Duree const& lhs, Duree const& rhs)
{
    return lhs.secondes < rhs.secondes;
}

bool operator>(Duree const& lhs, Duree const& rhs)
{
    return lhs.secondes > rhs.secondes;
}

bool operator<=(Duree const& lhs, Duree const& rhs)
{
    return !(lhs > rhs);
}

bool operator>=(Duree const& lhs, Duree const& rhs)
{
    return !(lhs < rhs);
}

std::ostream& operator<<(std::ostream& os, Duree const & duree)
{
    auto [heures, minutes, secondes] = en_heures(duree);
    
    auto format = [](int i)
    {        
        auto res = std::to_string(i);
        if(std::size(res) == 1)
            res = '0' + res;
        return res;
    };
    
    return os << heures << ':' << format(minutes) << '\'' << format(secondes) << '"';
}