#include <string>
#include <map>
#include <vector>

/* Data structure

discography 
    - artist -> name
        - album -> name
            - song -> name

*/

using namespace std;

// artist-- album----- songs----------- 
using discography = map<string, map<string, vector<string>>>;