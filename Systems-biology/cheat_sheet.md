# Biologie des systèmes
La biologie des systèmes s'exprime au niveau moléculaire, cellulaire, etc.  
L'objectif à terme est de produire <mark>un modèle mathématique  qui illustre les interactions</mark> complexes rencontrer dans les systèmes biologiques.
![1](1.PNG)

# Origine des informations
Les différentes informations se trouve sur une <mark>Uniprot</mark>, <mark>PFAM</mark> , <mark>Gene Ontlogy</mark>, <mark>SCOP</mark> / <mark>CATH</mark>, etc.

# Création d'un modèle
- Définir <mark>les membres du modèle</mark> et son organisation :
    - Paramètres suffisant pour être explicatif
    - En accord avec les données expérimentales
    - Prédictif
- Définir <mark>le fonctionnement du modèle</mark> :
    - Réactions réversibles (ou non)
    - Directionnalité
- Connaître <mark>les paramètres cinétique et thermodynamique</mark> ::
    - Existants
    - A calculer / inférer

![2](2.PNG)

Approche <mark>"top to bottom"</mark> / approche en entonnoir : <mark>définition des processus généraux en premier puis des détails</mark>.  
L'approche <mark>"bottom to top"</mark> permet de partir <mark>du minimum du système jusqu'à quelque chose de plus avancée</mark>.
# Gene Ontology
C'est un projet bio-informatique destiné à <mark>la structure et la description des gènes et des produits géniques</mark> dans le cadre d'une onthologie (En informatique et en science de l'information, une ontologie est l'ensemble structuré des termes et concepts représentant le sens d’un champ d'informations, que ce soit par les métadonnées d'un espace de noms, ou les éléments d'un domaine de connaissances) commune à toutes les espèces.
- <mark>Gérer et enrichir le vocabulaire</mark> décrivant les gènes et leurs produits.
- <mark>Gérer les annotations</mark> c'est-à-dire les informations rattachées au gène et a leur produit.
- <mark>Fournir les outils permettant d'accéder aux informations</mark> dans le cadre du projet.
la base de données GO est conçu comme <mark>un graphe orienté acyclique</mark> chaque terme étant en relation avec un ou plusieurs termes du domaine et parfois d'autres domaines.

## Processus biologique
Le processus biologique <mark>désigne un objectif biologique</mark> auquel le gène ou le produit génique contribue.  
Un processus est accompli par un ou plusieurs ensembles ordonnés de fonctions moléculaires.  
Les processus impliquent souvent une transformation chimique ou physique, en ce sens que quelque chose entre dans un processus et quelque chose de différent en sort. 

## Fonction moléculaire
<mark>La fonction moléculaire est définie comme l'activité biochimique</mark> (y compris la liaison spécifique à des ligands ou à des structures) d'un produit génique.  
Cette définition s'applique également à <mark>la capacité qu'un produit génique</mark> (ou un complexe de produits géniques) porte en tant que potentiel.  
<mark>Elle décrit uniquement ce qui est fait sans préciser où ou quand l'événement se produit réellement</mark>. 

## Compartiment cellulaire
Le terme "composant cellulaire" désigne <mark>l'endroit de la cellule où un produit génique est actif</mark>.  
Ces termes reflètent notre compréhension de la structure des cellules eucaryotes. 

## Avantages
- Permet de <mark>relier des processus biologiques entre eux</mark>. 
- Permet de <mark>faire le lien entre différents organismes</mark>. 
- Permet de <mark>relier des gènes et des protéines dans des processus complexes</mark>.
- Permet de prendre en compte <mark>la localisation cellulaire des protéines</mark>.
- Organisation semi-hiérarchique (du général au spécifique).
- Quelques <mark>déductions possibles (inférences)</mark>.

## Inconvénients
- <mark>Pas de directionnalité</mark> (pas complète et définitive).
- <mark>Pas de constantes cinétiques ou de concentration</mark>.
- <mark>Pas de description spécifique des réactions</mark>.
- Rarement détaillé au niveau de la protéine .
- Pas (peu) de prise en compte des métabolites.

# System Biology Graphical Notation SBGN
Permet <mark>la standardisation des notations graphiques</mark> utilisés dans les réseaux illustrant des processus biologiques.  
Permet, également, de <mark>représenter une réaction d'un système a vers un système b, même si les intermédiaires sont inconnus</mark>. La somme de tout se qui se passe entre les deux systèmes sans avoir le détail de ce qui se passe peut être obtenu.
Il est plus accès sur la visualisation et est utilisé pour faire face à la complexité des <mark>réseaux biochimiques avec une connaissance incomplète ou indirecte des détails biochimiques</mark>. 
Il est basé sur des graphes :
- <mark>Noeud</mark> : un point terminant une interaction
- <mark>Arête</mark> : une ligne entre deux noeuds
- <mark>Arc</mark> : une arête orienté
- <mark>Glyphe</mark> : un symbol donnant des information
- <mark>Graphe</mark> : nn ensemble de nœud connectés dans un graphe.


## Limites
<mark>Duplication des éléments qui interviennent plusieurs fois</mark> alors que c'est le même élément qui intervient tout au long d'un processus, par exemple une protéine.

# Systems Biology Markup Language SBML
Le Systems Biology Markup Language est <mark>un format de représentation, basé sur XML, de communication et de stockage de modèles de processus biologiques</mark>. C'est un standard libre et ouvert avec une prise en charge par de nombreux logiciels et développeurs.
Il est plus basé sur <mark>la description des paramètres</mark>.

# Systems Biology categories
From the FBA to Beyond metabolism to molecular biology.
1. Network reconstruction, ex : FBA (flux balance analysis)
2. Predictions using GEMS (Genome-Scale Metabolic Models) : FVA : flux variability analysis
3. Model improvement
4. Quantitative phenotype predictions
5. Multi-omic data integration
6. Beyond metabolism to molecular biology

## FBA Flux Balance Analysis
Approche mathématique pour l'analyse de flux métabolique dans un réseau à partir de réactions métaboliques connues et genes codant des enzymes. On essai de suivre le déplacement d'un métabolite dans le réseau.  
<mark>Permet de construire des contraintes pour le modèle et d'outrepasser des information biochimiques par l'utilisation d'une matrice creuse</mark> (avec m composants et n réactions). Chaque case de la matrice est un vecteur Sv, <mark>l’ensemble des vecteurs définisse une fonction objective qu’il faut minimiser</mark> (méthode de la plus grande pente par exemple) pour obtenir une solution optimale : optimisation.
Il peut y avoir des problèmes de convergence : la solution optimale n'existe, alors, pas.

### Goal
- <mark>Prédire le taux de croissance d'un organisme</mark>.
- <mark>Prédire le taux de production d'un métabolite</mark> biotechnologiquement important.
- Permet de voir un réseau global.
- Permet de voir si il y a des autoroutes ou des voies secondaires pour un processus métabolique.

![3](3.PNG)


## FVA Flux Variability Analysis
Une question clé qui peut se poser lors de l'utilisation de ces modèles est <mark>l'existence d'autres solutions optimales dans lesquelles le même objectif maximal (par exemple, le taux de croissance) peut être atteint avec différentes distributions de flux</mark>.  
Pour obtenir un optimum, <mark>plusieurs métabolites sont étudiés indépendamment</mark>.
Plusieurs flux peuvent mener au même résultat (même consommation et production de métabolites) :
- <mark>Redondance du réseau</mark>.
- <mark>Robustesse</mark>.
- <mark>Adaptation aux conditions physico-chimiques externes</mark>.

### Goal
- Permet de voir <mark>la sensibilité locale</mark>.
- Intéressant pour trouver <mark>les genes impliqués (knock out)</mark> et <mark>les sous réseaux importants</mark>. 


![4](4.PNG)

Cette image représente <mark>un cycle dit futile</mark>, il peut servir à <mark>stocker des métabolites</mark>, c'est un mécanisme de stockage de produit en masse dans les cellules, utilisé indépendament du cycle, cela peut également servir à <mark>détoxifier ou temporiser</mark>.

## Qu'est-ce-qui peut être fait en biologie des systèmes
- <mark>Déterminer une réaction contrôlée à petite échelle</mark>.
- <mark>Actualiser pour qu'il soit à l'échelle génomique</mark>.
- <mark>Définir les bornes (petites ou étroites)</mark>.
- Utiliser différents solveurs pour évaluer son comportement en définissant une cible (utiliser une fonction objective).
- <mark>Un modèle basé sur la constrainte</mark>.
- <mark>Prédire ou confirmer certains comportements</mark> (plusieurs chemins, robustess, knockout de gènes, étude sous-optimale).

# Annales
> Soit les réactants A et B d’une réaction aboutissant à la formation d’un produit C. Ecrivez la réaction et discutez les éléments à prendre en compte pour définir au mieux ce système et son comportement.  

A + B -> C, réaction réversible ou non, est-ce-qu'il y a des partenaires, stoechométrie et vitesse de réaction à prendre en compte etc.

> indiquez quelques exemples des différents formats de représentation des données et réseaux d’interaction utilisés en biologie des systèmes, à quoi servent-ils, quelles sont leurs forces et leurs faiblesses ?

BioPAX, PSI-MI, SBML et CellML

> soit la matrice M suivante qui définit les poids des réactions et métabolites d’un système d’intérêt.  

C'est une sparse matrice, matrice à trou/creuse, on l'a remplie en combinant FBA et FVA, en faisant varier les flux ou les poids.

> une équipe veut étudier les voies métaboliques d’une bactérie en faisant appel à des données issues de spectrométrie de masse (qualitatives et quantitatives) sur plus de 2000 protéines identifiées chacune par leur nom de gène.
1) Indiquez les informations et sites qu’ils pourront utiliser pour reconstruire le réseau métabolique à partir de ces données.
2) Quelles vont être les limitations prévisibles du modèle ?
3) Si ce modèle est implémenté avec cobrapy, que pourra-t’on modéliser ?

1. > Quelles sont les étapes à réaliser pour obtenir un modèle d’un système d’étude ? Détaillez les acronymes des méthodes et indiquez le rôle de chacune de ces étapes.

> Écrire les réactions avec les équations du système comprenant les métabolites ABCDE, en supposant les stœchiométries conservées à l’identique pour toutes les réactions : 4A, 3B, 5C, 2D, 3E. On prendra A comme entrée et E comme sortie.  

> Si R2, R4 et R5 sont des réactions réversibles, comment appelle-t’on ce système ? A quoi sert-il dans une voie de signalisation biologique ?  

C'est un __cycle futile__. Voie de stockage du composé C, permet de temporiser une réaction.

> Sans décrire forcément en détail le système linéaire (on supposera une loi d’action de masse simple pour toutes les réactions avec une vitesse de réaction identique pour chaque réaction), quels sont les métabolites qui vont augmenter / diminuer au cours du temps ? Justifiez vos réponses.  

Globalement, il va y avoir une création de E.

> A genome-scale model (GSM) provides an inventory of reactions for a given organism that allows for the analysis of cellular metabolism and the design of gene modulation strategies for bioproduction. Despite extensive manual curations, thermodynamically infeasible cycles (TIC) often exist in GSMs because of overly permissive reaction inclusion or directionalities that can affect flux range calculations using flux balance analysis (FBA) and/or flux variability analysis (FVA). A TIC is an internal cycle in the metabolic network satisfying mass balances and directionality constraints without involving any exchange reactions, i.e. no system input/output is required. For example, a TIC is formed by the following three reactions:  
H 2 O + Glutamate + NAD + ↔ H + + NH + 4 + NADH + 2-Oxoglutarate  
Alanine + 2-Oxoglutarate ↔ Pyruvate + Glutamate  
H 2 O + Alanine + NAD + ↔ H + + Pyruvate + NH +4 + NADH  
The cycle can carry an arbitrarily large flux when performing FBA despite the fact that any turn around the cycle does not produce or consume any metabolites. Thus, it violates the second law of thermodynamics as the net change of Gibbs free energy is zero. Quelle(s) stratégie(s) pouvez-vous mettre en place pour permettre de résoudre « l’infaisabilité » d’un modèle construit à l’échelle d’un génome ?*  

