# Initialisation du projet

```bash
django-admin startproject mysite
pip install psycopg2
pip install django-debug-toolbar
# Les trois commandes suivantes vont permettre de créer une base de données psql
sudo -u postgres psql -c "create simon disquaire;" # Ou faire la commande suivante
sudo -i -u postgres psql # + la commande suivante, une fois dans le terminal psql
createdb -O simon disquaire
\l
\q
```

```
disquaire/
    manage.py
    disquaire/
        __init__.py
        settings.py
        urls.py
        asgi.py
        wsgi.py
```

- Le premier répertoire racine disquaire/ est un contenant pour votre projet. Son nom n’a pas d’importance pour Django ; vous pouvez le renommer comme vous voulez.
- **manage.py** : <mark>un utilitaire en ligne de commande qui vous permet d’interagir avec ce projet Django de différentes façons</mark>. Vous trouverez toutes les informations nécessaires sur manage.py dans django-admin et manage.py.
- Le sous-répertoire disquaire/ correspond au paquet Python effectif de votre projet. C’est le nom du paquet Python que vous devrez utiliser pour importer ce qu’il contient (par ex. disquaire.urls).
- disquaire/__init__.py : un fichier vide qui indique à Python que ce répertoire doit être considéré comme un paquet. Si vous êtes débutant en Python, lisez les informations sur les paquets (en) dans la documentation officielle de Python.
- **disquaire/settings.py** : <mark>réglages et configuration de ce projet Django</mark>. Les réglages de Django vous apprendra tout sur le fonctionnement des réglages.
- **disquaire/urls.py** : <mark>les déclarations des URL de ce projet Django, une sorte de « table des matières » de votre site Django</mark>. Ceux sont toutes les routes et chemins, permettant d'accéder à une page d'une application. Vous pouvez en lire plus sur les URL dans Distribution des URL.
- **disquaire/asgi.py** : un point d’entrée <mark>pour les serveurs Web compatibles aSGI</mark> pour déployer votre projet. Voir Comment déployer avec ASGI pour plus de détails.
- **disquaire/wsgi.py** : un point d’entrée <mark>pour les serveurs Web compatibles WSGI</mark> pour déployer votre projet. Voir Comment déployer avec WSGI pour plus de détails.

Dans **disquaire/setting.py**, changer la base de données utilisée par défaut par postgresql

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql', # Adaptateur postgresql
        'NAME': 'learning_database', # Nom de la bdd
        'USER' : 'simon',
        'PASSWORD' : '',  
        'HOST' : '',
        'POST' : '5432',
    }
}

# De plus, on peut changer la langue et le fuseau horaire utilisés :

LANGUAGE_CODE = 'fr'

TIME_ZONE = 'Europe/Paris'
```

Effectuez les commandes suivantes pour effectuer les migrations conseillées par Django.

```bash
cd learning_database 
./manage.py runserver # Lance le serveur django
./manage.py migrate # Effectue les migrations
```

L'url indiqué sur la console est l'adresse du serveur : http://127.0.0.1:8000/

# Django-debug-toolbar
Django-debug-toolbar est une application facilitant le débugage sur serveur.

```bash
pip install django-debug-toolbar
```

settings.py :
```python
INSTALLED_APP = [
    "..."
    'debug_toolbar',
]

MIDDLEWARE = [
    "..."
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

INTERL_IPS = ["127.0.0.1"]
```

urls.py

```python
from django.conf import settings
from django.conf.urls import url, include

if settings.DEBUG: # Si le mode debug est activité, on ajoute le mode debug aux url existants
    import debug_toolbar
    urlpatterns = [
        url(r"^__debug__/", include(debug(debug_toolbar.urls)),
    ] + urlpatterns
```

Sur notre site, la barre à droite correspond à l'application django-debug-toolbar.

# Creer une application
> Quelle est la différence entre un projet et une application ? **Une application** est <mark>une application Web qui fait quelque chose, qui a une nouvelle fonctionnalité</mark> – par exemple un système de blog, une base de données publique ou une petite application de sondage. **Un projet** est <mark>un ensemble de réglages et d’applications pour un site Web particulier</mark>. Un projet peut contenir plusieurs applications et une application peut apparaître dans plusieurs projets.
```bash
django-admin startapp store
```

- *migrations* contient les fichiers qui modifient la structure de la base de données.
- *admin.py* contient la configuration de l'espace d'administration.
- *apps.py* permet de configurer l'application.
- *model.py* contient les modèles de l'application.
- *test.py* contient les tests.
- *views.py* contient les vues de notre application.

L'application est à déclarer dans *settings.py* pour être utilisée :

```python
INSTALLED_APPS = [
    'gestionnaire.apps.GestionnaireConfig',
    "..."
]
```

# L'architecture MVT
Django s'appuie sur le modèle vue template MVT issu du Modèle Vue Contrôleur.

## Le modèle
> Le modèle interagit avec la base de données. Sa mission est de chercher dans une base de donnée les items correspondant à une requête et de renvoyer une réponse facilement exploitable par le programme.

<mark>Le modèle cherche dans une base de données les items correspondant à une requête</mark>. Il renvoie une réponse intelligible par le reste du programme. Il s'appuie sur un ORM (Object Relational Mapping). Pour interroger une base de données il faut utiliser SQL.  
L'ORM est comme un traducteur, il va permettre d'écrire un requête en python, de la traduire en SQL, puis d'intérroger une base de données, de fournir la réponse en SQL et de la traduire en python.


## Les templates
> Un template est un fichier HTML qui peut recevoir des objets Python et qui est lié à une vue (nous y reviendrons).
Ajouter un dossier template dans le projet :

```bash
mkdir learning_database/templates
mkdir learning_database/templates/learning_database
```

## La vue
<mark>Elle va recevoir une requête HTTP et renverra une réponse intelligible par le navigateur</mark>. Elle réalise toutes les actions nécessaire au traitement de la requête (appelle de template, recherche dans une base de données).
> Une fonction de vue, ou vue pour faire simple, est une fonction Python acceptant une requête Web et renvoyant une réponse Web. Cette réponse peut contenir le contenu HTML d’une page Web, une redirection, une erreur 404, un document XML, une image… ou vraiment n’importe quoi d’autre.

### Ajouter une vue
Ajoutez 2 dossiers, tel que: 
```bash
mkdir store/templates
mkdir sotre/templates/store
``` 

Il est d'usage de séparer les urls d'une application des urls du projet. Il y a *store/urls.py* qui est chargé de s'occuper des routes de l'application et il est importé dans *urls.py* qui est chargé lui, de s'occuper de toutes les routes des applications.

Créez alors un fichier *store/urls.py*
```bash
mkdir store/urls.py
```

*urls.py* :

```python
urlpatterns = [
    url(r"^store/", include("store.urls")),
    url(r"^admin/", admin.site.urls),
]
```

La méthode `include` permet d'inclure les urls de l'application. la fonction url associe un schéma (expression régulière) de route à un ensemble de vues.

*store/views.py* :
```python
from django.http import HttpResponse

def index(request):
    message = "Salut tout le monde!"
    return HttpResponse(message)
```

`HttpResponse` permet de renvoyer une réponse interprétable par un navigateur.

Créer *store/urls.py* et ajouter :

```python
from django.conf.urls import url
from . import views # import views so we can use them in urls.
urlpatterns =  [
    url(r"^$", views.index),
]
```

Ajouter dans *urls.py* :

```python
from gestionnaire import views

urlpatterns = [
    url(r"^$", views.index),
    url(r"^store/", include("store.urls")),
    path('admin/', admin.site.urls),
]
```

Il ne faut pas oublier de supprimer la route des url de l'application dans *store/urls.py*, sinon la vue sera importée deux fois :

```python
urlpattenrs = [
    # url(r"^$", views.index),
]
```

Pour visualiser le résultat, on créer temporairement des dictionnaires *store/models.py* :

```python
ARTISTS = {
    "francis-cabrel" : {"name":"Francis Cabrel"},
    "lej":{"name":"Elijay"},
    "rosana":{"name":"Rosana"},
    "maria-dolores-pradera":{"name":"Maria Doloroes Pradera"},
}

ALBUMS = [
    {"name":"Sarbacane", "artists":[ARTISTS["francis-cabrel"]]},
    {"name":"La Dalle", "artists":[ARTISTS["lej"]]},
    {"name":"Luna Nueva", "artists":[ARTISTS["rosana"],ARTISTS["maria-dolores-pradera"]]},
]
```

Creer une nouvelle route dans les urls de l'application *store/urls*.py :

```python
urlpatterns = [
    url(r"^$", views.listing),
]
```

Cette nouvelle vue n'existant pas, il faut alors la créer dans *store/views.py* :

```python
from .models import ALBUMS # Import de la constante ALBUMS

def listing(request):
    albums = ["<li>{}</li>".format(album["name"]) for album in ALBUMS]
    message = """<ul>{}</ul>""".format("\n".join(albums))
    return HttpsResponse(message)
```

### Passer des arguments à une vue
3 manières de transmettre des informations :
- A l'intérieur de l'URL. (site/10)
- A l'intérieur de la requête GET (paramètre).
- Dans le corps d'une requête POST.


#### A l'intérieur d'un url
Créer une nouvelles fonctions dans *store/views.py* :

```python
def detail(request, album_id):
        id = int(album_id) # Convert l'album id
        album = ALBUMS[id]
        artists = " ".join([artists["name"] for artist in album["artists"]])
        message = "Le nom de l'album est {}. Il a été écrit par {}".format(album["name"], artists)
        return HttpResponse(message)
```
Maintenant que cette vue est crée, il faut l'ajouter aux routes, *store/urls.py*

```python
urlpatterns = [
    "..."
    url(r"^(?P<album_id>[0-9]+)/$", views.details),
]
```

Les motifs d'urls sont simplement la forme générale d'une url.

#### Dans une requête `GET`
Dans une méthode get, les informations de l'utilisateur sont rattachéesà query dans l'url : site.com/search?query=rosana
On pourrait alors ajouter une route dans l'urlpatterns comme ceci : `'^search/\?query=(?P<query>[A-Z][a-z]+)/$'`. Mais ça ne serait pas pratique, si il y a plusieurs terme etc, certaines erreurs ne seraient pas capturées. 

Ajout d'une nouvelle vue search dans *store/views.py* :

```python
def search(request):
    query = request.GET.get("query")
    if not query: # Si le dictionnaire est vide
        message = "Aucun artiste n'est demandé"
    else:
        albums = [
            album for album in ALBUMS
            if query in " ".join(artists["name"] for artist in album["artists"])
        ]

        if len(albums) == 0:
            message = "Misère de misère, nous n'avons trouvé aucun résultat !"
        else:
            albums = ["<li>{}</li>".format(album("name"]) for album in ablums]
            message = """
                Nous avons trouvé les albums correspondant à votre requête ! Les voici : 
                <ul>
                    {}
                </ul>
            """.format("</li><li>".join(albums))
        
        return HttpResponse(message)
```

Request est une instance de la classe wsgi.request, ainsi `request.GET` renvoie tous les paramètres passés dans la requête sous forme de dictionnaire.

L'objet request est une instance de la class wsgi.request (il régle les réglages du serveurs web), notamment `GET` qui renvoie tous les paramètres passés dans la requête dans un dictionnaire.

Puis on la relie à une schéma d'url dans gestionnaire/urls.py :

```python
urlpatterns = [
    "..."    
    url(r"^search/$", views.search),
]
```

# Créer les tables SQL
Faire un diagramme SQL avec les relations

L'héritage `model.Model` permet de faire le lien entre la représentation d'une table en python et la table en elle même en SQL.  
Le code ci-dessous, va ajouter les relations dans la base de données.
Modifiez dans *store/models.py* :

```python
from django.db import models


class Artist(models.Model):
    name = models.CharField(max_length=200, unique=True) # Equivalent à :
    # CREATE TABLE store_artist ( "id" serial NOT NULL PRIMARY KEY, "name" varchar(200) NOT NULL UNIQUE );

class Contact(models.Model):
    email = models.EmailField(max_length=100)
    name = models.CharField(max_length=200)


class Album(models.Model):
    reference = models.IntegerField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    available = models.BooleanField(default=True)
    title = models.CharField(max_length=200)
    picture = models.URLField()
    artists = models.ManyToManyField(Artist, related_name='albums', blank=True)

class Booking(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    contacted = models.BooleanField(default=False)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    album = models.OneToOneField(Album)
```

# Les migrations
Une migration est un fichier contenant des instructions modifiant la structure d'une base de données. Elle permettent d'éviter les bases de données corrompues. Django n'applique pas les modifications qui vont générer des erreurs.
Les migrations s'apparentent à un historique git avec tous les changements effectués depuis le début sur la base de données.

Pour faire des migrations automatiques :

```bash
./manage.py makemigrations
./manage.py migrate
```

Afin de vérifier que les tables ont bien été créé:

```bash
psql disquaire
\dt
```

## Etapes à suivre pour ajouter ou modifier un modèle
1. Ouvrir le fichier *models.py* et le modifier.
2. Générer une nouvelle migration : *manage.py* makemigrations.
3. Lire et contrôler la migration.
4. Lancer la migration : *manage.py* migrate.

# La console Django
Ici, on utilise la console pour créer de nouveaux items dans la base de données.

```bash
./manage.py shell
from gestionnaire.models import Artist, Album
patrick = Artists(name="Patrick Bruel")
patrick.save()
patrick.name = "Patrick on t'aime"
patrick.save()
patricke.delete()
Artist.objects.create(name="Francis Cabrel")
francis = Arists.objects.get(name="Francis Cabrel")
francis

album = Album.objects.create(title="Sarbacane")
album.artists.add(francis)
album.artists
Artist.objects.all() # Equivalent à une liste avec + de méthodes
Artist.objects.filter(name="Francis Cabrel")
```

Pour controller l'affichage, il faut ajouter une méthode `__str__` dans les classes du *store/models.py* :

```python
class Artist(models.Model):

    def __str__(self):
        return self.name


class Contact(models.Model):

    def __str__(self):
        return self.name


class Album(models.Model):

    def __str__(self):
        return self.title


class Booking(models.Model):

    def __str__(self):
        return self.contact.name
```

# Trouver des données
Ajouter à la fonction `index` dans *store/views.py* :
```python
from .models import Album, Artist, Contact, Booking

def index(request):
    # request albums
    albums = Album.objects.filter(available=True).order_by('-created_at')[:12]
    # then format the request.
    # note that we don't use album['name'] anymore but album.name
    # because it's now an attribute.
    formatted_albums = ["<li>{}</li>".format(album.title) for album in albums]
    message = """<ul>{}</ul>""".format("\n".join(formatted_albums))
    return HttpResponse(message)

# Affiche tous les albums
def listing(request):
    albums = Album.objects.filter(available=True)
    formatted_albums = ["<li>{}</li>".format(album.title) for album in albums]
    message = """<ul>{}</ul>""".format("\n".join(formatted_albums))
    return HttpResponse(message))

# Page de détail d'un album
def detail(request, album_id):
    album = Album.objects.get(pk=album_id)
    artists = " ".join([artist.name for artist in album.artists.all()])
    message = "Le nom de l'album est {}. Il a été écrit par {}".format(album.title, artists)
    return HttpResponse(message)

def search(request):
    query = request.GET.get('query')
    if not query:
        albums = Album.objects.all()
    else:
        # title contains the query and query is not sensitive to case.
        albums = Album.objects.filter(title__icontains=query)

    if not albums.exists(): # Si un aritste est passé en paramètre on va chercher tous les ablums de l'artiste passé en paramètre
        albums = Album.objects.filter(artists__name__icontains=query)

    if not albums.exists():
        message = "Misère de misère, nous n'avons trouvé aucun résultat !"
    else:
        albums = ["<li>{}</li>".format(album.title) for album in albums]
        message = """
            Nous avons trouvé les albums correspondant à votre requête ! Les voici :
            <ul>{}</ul>
        """.format("</li><li>".join(albums))

    return HttpResponse(message)]
```

# Ajouter des fichiers stastiques
Un thème est un ensemble de fichiers qui ont pour objectif de changer l'apparence d'un contenu. Il est statique car il ne va pas changer en fonction du contenu.
Un gabarit est un fichier HTML pouvant interpréter du contenu python pour générer du HTML.


Pour intégrer un thème dans une application Django :
1. Analyser les différences entre le thème et les maquetes
2. Intégrer les fichiers statiques
3. Adapter le HTML fourni par le thème


Créer un dossier static à l'intérieur de l'application :
```bash
mkdir gestionnaire/static
mkdir gestionnaire/static/gestionnaire
```

Il faut y placer tous fichiers sauf le dossier html.

Par défaut Django cherche les gabarits disponibles dans le dossier templates de notre applications.

Créer un fichier html :
```bash
touch gestionnaire/templates/index.html
```

importer le module loader dans views.py :
```python
from django.template import loader


def index(request):
    membres = Membres.objects.order_by("-name")
    formatted_membres = ["<li>{}</li>".format(membre.name) for membre in membres]
    template = loader.get_template("gestionnaire/index.html")
    return HttpResponse(template.render(request=request))
```

Refaire les liens css dans la page html :

```html
{% load static %}

<link rel="stylesheet" type="text/css" href="{% static 'gestionnaire/css/style.css' %}"/>

<script src="{% static 'gestionnaire/js/script.js' %}"></script>
```

## Organiser les gabarits
Problèmatique : lorsque l'on va créer de nouveaux templates et qu'ils vont être associer aux vues, le contenu HTML va être répété.

Pour ce faire on va utiliser un gabarit de base qui sera appelé dans les autres gabarits.

Héritage de gabarit : un gabarit enfant peut hériter d'un gabarit parent.

```html
touch gestionnaire/templates/gestionnaire/base.html
```

Supprimer tout le contenu de index.html et ajouter :
```html
{% extends 'store/base.html' %}
```

Indiquer dans le fichier base.html qu'une zone peut être amener à varier 
```html
<div class="container">
    <div class="row">
        <div class="box">
            {% block content %}{% endblock %}
        </div>
    </div>
</div>
```

Puis dans le gabarit enfant on peut insérer du contenu à l'intérieur (index.html) :

```html
{% extends 'gestionnaire/base.html' %}

{% block content %}
Et là on peut rajouter du contenu!!
{% endblock %}
```

## Inclure un gabarit dans un autre
Créer le fichier suivant templates/gestionnaire/list.html et ajouter dedans :

```html
{% extends 'gestionnaire/base.html' %}

{% block content %}
Et là on peut rajouter du contenu!!

{% include 'gestionnaire/list.html' %}
{% endblock %}
```

Modifier views.py :
```python
def index(request):
    membres = Membres.objects.order_by("-name")
    formatted_membres = ["<li>{}</li>".format(membre.name) for membre in membres]
    template = loader.get_template("gestionnaire/index.html")
    context = {"membres": membres:}
    return HttpResponse(template.render(context, request=request))
```

Ajouter dans list.html :
```html
<ul>
    {% for membre in membres %}
    <div class="col-sm-4 text-center">
        <a href="/">
            <img class="img-responsive" src="{{ membres.photo }}" alt="{{ album.titre }} ">
        </a>
        <h3><a href="/">{{ membre.name }}</a></h3>
        {% for artist in album.artists.all %}
            <p>{{ artist.name }}</p>
        {% endfor %}
    </div>
    {% if forloop.counter|divisibleby:3 %}<div class="clearfix"></div>{% endif %}
    {% endfor %}
</ul>
```


`{% if forloop.counter|divisibleby:3 %}` est un filtre de balise de gabarits.

## Ajouter des gabarits manquants
La balise de gabarit url, va prendre en paramètr ele nom de la vue.
Pour se faire ajouter à gestionnaire/urls.py :

```python
urlpatterns = [
    url(r"^$", views.listing, name="listing"),
    url(r"^(P<album_id>[0-9]+)/$", views.detail, name="detail")
    url(r"^search/$", views.search, name="search")
]
```

On peut maintenant renseigner l'url dans une page html par exemple :

```html
<ul>
    {% for membre in membres %}
    <div class="col-sm-4 text-center">
        <a href="{$ url 'gestionnaire:detail' album_id=album.id %}">
            <img class="img-responsive" src="{{ membres.photo }}" alt="{{ album.titre }} ">
        </a>
        <h3><a href="{$ url 'gestionnaire:detail' album_id=album.id %}">{{ membre.name }}</a></h3>
        {% for artist in album.artists.all %}
            <p>{{ artist.name }}</p>
        {% endfor %}
    </div>
    {% if forloop.counter|divisibleby:3 %}<div class="clearfix"></div>{% endif %}
    {% endfor %}
</ul>
```

Ajout de l'espace de nommage dans l'urls.py du projet :

```python
urlpatterns = [
    url(r"^$", views.index),
    url(r"^gestionnaire/", include("gestionnaire.urls", namespace="store")),
    path('admin/', admin.site.urls),
]
```

Ajout d'un gabarit manquant, modifier views.py :
```python
def index(request):
    membres = Membres.objects.order_by("-name")
    formatted_membres = ["<li>{}</li>".format(membre.name) for membre in membres]
    context = {
        "membres":membre
    }
    return render(request, "gestionnaire/index.html", context)


def search(request):
    query = request.GET.get("query")
    if not query:
        membres = Membres.objects.all()
    else:
        membres = Membres.objects.filter(name__icontains=query)

        if not membres.exists:
            message = "Misère de misère, nous n'avons trouvé aucun résultat !"
        
        # si un artiste était passé au lieu d'un album
        # albums = Album.objects.filter(artists__name__iconaints=query)

        else:
            membres = ["<li>{}</li>".format(membre.title) for album in ablums]
            message = """
                Nous avons trouvé les albums correspondant à votre requête ! Les voici : 
                <ul>
                    {}
                </ul>
            """.format("</li><li>".join(albums))
    context = {
        "name":name
    }  
        
    return render(request, "gestionnaire/search.html", context)
```

Renseigner les valeurs de certaines variables utilisées dans les gabarits inclus, search.html :
```html
{% extends 'gestionnaire/base.html' %}

{% block content %}
Et là on peut rajouter du contenu!!

{% include 'gestionnaire/search.html' with list_title=title %}
{% endblock %}
```

Supprimer la structure conditionnelle dans views.py car elle sera beaucoup plus pertinente dans la partie html :
list.html

```html
{% if albums|length_is:"0" %}
    <div class="texte-center">
    Aucune requête trouvée
    </div>
{% else %}
    {% for membre in membres %}
    <div class="col-sm-4 text-center">
        <a href="{$ url 'gestionnaire:detail' album_id=album.id %}">
            <img class="img-responsive" src="{{ membres.photo }}" alt="{{ album.titre }} ">
        </a>
        <h3><a href="{$ url 'gestionnaire:detail' album_id=album.id %}">{{ membre.name }}</a></h3>
        {% for artist in album.artists.all %}
            <p>{{ artist.name }}</p>
        {% endfor %}
    </div>
    {% if forloop.counter|divisibleby:3 %}<div class="clearfix"></div>{% endif %}
    {% endfor %}
{% endif %}
```

# la vue erreur!
Pour voir les pages à l'oeuvre, il faut mettre DEBUG = False dans settings.py


Changer dans views.py :
```python
from django.shortcuts import get_object_or_404

def detail(request, name):
    membre = get_object_or_404(Membres, pk=name)
```

Créer un fichier html dans gestionnaire/templates/404.html avec base.html et un block nouveau.

# Formulaire de recherche
Créer un template/gestionnaire/search_form.html :
```html
<div class="col-lg12 detail-separator">
    <form class="col-md-6 col-md-offset-3 text-center" action="{% url 'gestionnaire:search' %}" method="get" accept-charset="utf-8">
        <div class="form-group">
            <label for="searchForm">Chercher un disque</label>
            <input id="searchForm" class="form-control" name="query">
        </div>
        <span class="help-block" id="helpBlock">Trouvez le CD de vos rêves en tapant son titre dou le nom d'une des artistes.</span>
    </form>
</div>
```

Mettre à jour list.html pour y inclure le formulaire de recherche :
{% include "gestionnaire/search_form;html" %}

# La pagination
Dans views.py :
```python
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


def listing(request):
    albums_list = Album.objects.filter(available=True)
    paginatore = Paginator(albums_list, 9)
    page = request.Get.get("page") # Pour avoir le numéro de page que consulte l'utilisateur
    try :
        albums = paginator.page(page)
    except PageNotAnInteger:
            # if page is not an integer, deliver first page.
            albums = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        albums = paginator.page(paginator.num_pages)
    context = {
        "albums": albums
    }
    return render(request, "gestionnaire/listing.html", context)
```

## Ajout de navigation de page
Modifier list.hmtl :
```html

{% if paginate %}
    {% if albums.has_previous %}
        <li><a href="?page={{ albums.previous_page_number }}">Précédent</a></li>
    {% endif %}
    {% if albums.has_next %}
        <li>a href="?page={{ albums.next_page_number }}">Suivant</a></li>
    {% endif %}
{% endif %}
```

Ajouter dans views.py
```python
def listing(request):
    albums_list = Album.objects.filter(available=True)
    paginatore = Paginator(albums_list, 9)
    page = request.Get.get("page") # Pour avoir le numéro de page que consulte l'utilisateur
    try :
        albums = paginator.page(page)
    except PageNotAnInteger:
            # if page is not an integer, deliver first page.
            albums = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        albums = paginator.page(paginator.num_pages)
    context = {
        "albums": albums,
        "paginate": True
    }
    return render(request, "gestionnaire/listing.html", context)
```

# Formulaire
Il faut relier le formulaire à une url, dans detail.html:

```html
<form class="form-inline" action="{% url 'gestionnaire:detail' album_id=album_id %}" method="psot">
{% crsft_token %}
"Les labels et inputs"
<!-- <input="hidden" class="hidden" value="{{ album_id }}" name="album_id"> -->
"Puis le boutons"
```

Changer le comportement entre post et et get, dans views.py :
```python
 
```
Il faut alors créer le gabarit associer...
Dans gestionnaire/forms.py :
```python
from django import forms

class ContactForm(forms.Form):
    #name = forms.CharField(max_length=100)
    # Cette ligne génère :
    # <input type="text" name="name" maxlength="100" id="id_name">

    name = forms.CharFields(
        label="Nom",
        maxmax_length=100,
        widget=form.TextInput(attrs={"class": "form-control"}),
        required=True
    )
    email = form.EmailField(
        label="Email",
        widget=forms.EmailInput(attrs={"class": "form-control"}),
        required=True
    )
```

Dans views.py, ajouter le formulaire de contact :
```python
from .forms import ContactForm

"..."

def detail(request, album_id):
    album = get_object_or_404(Album, pk=album_id)
    artists_name = " ".join([artist.name for artist in album.artists.all()])
    context = {
        "labum_title":album.title,
        "artists_name": artists_name,
        "album_id":album.id,
        "thumbnail":album.picture
    }
    if request.methjod == "POST":
        form = Contactform(request.POST)
        if form.is_valid():
                email = form.cleaned_data["email"]
                name = form.cleaned_data["name"]

            contat = Contact.objects.filter(email=email)
            if not contact.exists():
                #If a contact is not registered, create a new one.
                contact = Contact.bojects.create(
                    email = email,
                    name = name
                )
            album = get_object_or404(Album, id=album_id)
            booking = Booking.objects.create(
                contact = contact,
                album = album
            )
            album.available = False
            album.save()
            context  = {
                "album_title": album.title
            }
            return render(request, "gestionnaire/merci.html", context)
        else:
            context["errors"] = from.errors.items()
    else:
        form = ContactForm()
        
    context = {
        "labum_title":album.title,
        "artists_name": artists_name,
        "album_id":album.id,
        "thumbnail":album.picture,
        "form":form 
    }
    return render(request, "gestionnaire/detail.html", context)
```

De ce fait il faut changer dans le gabarit detail.html:
```html
<div class="form-group {% if form.name.errors %}has-warning has-feedback{% endif %}">
    <label for="{{ form.name.id_for_label }}" calss="contr-label">Nom</label>
    {{ form.name }}
</div>
<div class="form-group {% if form.email.errors %}has-warning has-feedback{% endif %}">
    <label for="{{form.email.id_for_label }}" class="control-label">E-mail</label>
    {{ form.email }}
</div>
"..."
```

Dans detail.html, on va afficher les erreurs :
```html
"..."
{% if errors %}
    <div>
        {% for key, error in errors %}
        {% autoescape off %}
            {{ error }}
        {% endautoescpae %}
        {% endfor %}
    </div>
{% endif %}
```

## Améliorer le formulaire
Dans forms.py tout refaire :
```python
from django.forms import ModelForm, TextInput, EmailInput
from .models import ContactForm
from django.forms.utils import ErrorList

class ParagraphErrorList(ErrorList):
    def __str__(self):
        reutnr self.as_divs()

    def as_divs(self):
        if not self: return ''
        return '<div class="errorlist">%s</div>' % ''.join(['<p class="smallerror">%s</p>' % e for e in self])


class ContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = ["name", "email"]
        widget = {
            "name":TextInput(attrs={"class":"form-control"}),
            "email":EmailInput(attrs={"class":"form-control"})
        }
```

Ajouter dans views.py et changer `form = Contactform(request.POST)`:

```python
from .forms import ContactForm, ParagraphErrorList
"..."
        form = Contactform(request.POST, error_class)ParagraphErrorList)
```

# Eviter les bases corrompues
Dans views.py :

```python
from django.db import transaction, IntegrityError
@transaction.atomic
def detail...
        try:
            with:
                contat = Contact.objects.filter(email=email)
                if not contact.exists():
                    #If a contact is not registered, create a new one.
                    contact = Contact.bojects.create(
                        email = email,
                        name = name
                    )
                else:
                    contact = contact.first()
                album = get_object_or404(Album, id=album_id)
                booking = Booking.objects.create(
                    contact = contact,
                    album = album
                )
                album.available = False
                album.save()
                context  = {
                    "album_title": album.title
                }
                return render(request, "gestionnaire/merci.html", context)
        except IntegrityError:
            form.errors["internal"] = "Une erreur interne est apparue. Merci de recommencer votre requêtes."

    else:
        form = ContactForm()

    context["form"] = form
    context["errors"] = form.errors.items()
    return render(request, "gestionnaire/detail.html", context)
```

# Administration
Ajouter dans settings.py :

```python

MIDDLEWARE = [
    "..."
    "django.middleware.locale.LocaleMiddleware",
]
```

Puis changer l'url admin dans urls.py par un autre url (admin étant très facilement attaquable).


Creation d'un compte admin :
```bash
./manage.py createsuperuser
```


Faire apparaître les détails d'une table dans l'espace d'administration, admin.py
```python

from .models import Membres

admin.site.register(Membres)