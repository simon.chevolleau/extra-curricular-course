# Définition
Le CSS "Cascading Style Sheet" correspond à un langage informatique permettant de mettre en forme des pages web (HTML ou XML). Ce langage est donc composé des fameuses « feuilles de style en cascade » également appelées fichiers CSS (.css) et contient des éléments de codage.

# Syntaxe
Sélecteur va permettre de définir quel élément va être affecté par les styles qu'on va définir. Ainsi, un sélecteur aura des propriétés (couleurs etc) et une valeur qui leur est associée.

# Ecrire du CSS
Soit dans la partir en tête de l'HTML dans une balise style :
```html
<style>
h1 {
    color: red;
}
</style>
```

Soit directement dans les attributs d'une balise HTML :

```html
<h2 style="color:red">Titre</h2>
```

Ou, et c'est la meilleure façon de faire, en déclarant le CSS dans un fichier à part et en le liant au contenu HTML :

```html
<link href="style.css" rel="stylesheet">
```

```css
h2 {
    color: red;
}