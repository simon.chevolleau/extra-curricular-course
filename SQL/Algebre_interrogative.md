- [1. Project, sélection, renommage](#1-project-sélection-renommage)
- [2. La jointure](#2-la-jointure)
  - [2.1. Cas limite de la jointure : le produit cartésien x](#21-cas-limite-de-la-jointure--le-produit-cartésien-x)
- [3. Les opérations ensemblistes](#3-les-opérations-ensemblistes)
  - [3.1. L'union $`S \cup T`$](#31-lunion-s-cup-t)
  - [3.2. La différence $`S - T`$](#32-la-différence-s---t)
  - [3.3. L'insertion c'est une jointure](#33-linsertion-cest-une-jointure)
- [4. La composition](#4-la-composition)
- [5. Des opérations redondantes](#5-des-opérations-redondantes)
  - [5.1. Les sélections plus compliquées](#51-les-sélections-plus-compliquées)
  - [5.2. La division cartésienne](#52-la-division-cartésienne)
  - [5.3. La semi-jointure](#53-la-semi-jointure)
  - [5.4. La $`\theta - jointure`$](#54-la-theta---jointure)
- [6. Evaluations et optimisation](#6-evaluations-et-optimisation)
# 1. Project, sélection, renommage
Que peut-on faire avec une seule table ?
- **Sélection** : ne voir que certaines lignes satisfaisant une condition donnée $`\rightarrow`$ $`\sigma_{condition}(R) = { t \in R | t satisfait C}`$
    - $`attribut = | \neq valeur`$
    - $`attribut = | \neq attribut`$
- $`type(\sigma_{condition}(R)) = type(R)`$
- **Projection** : <mark>supprimer des colonnes</mark> $`\rightarrow`$ $`\pi_{A_1, ..., A_n}(R) = {t|{
A_1, ..., A_n}| t \in R}`$ <mark>Attention il peut y avoir des doublons.</mark>
    - $`{A_1, ..., A_n}`$ : sous-ensemble des attributs de R.
- **Renommer** : <mark>renommer les attributs</mark> $`\delta_{A\rightarrow A', D \rightarrow E}(E)`$
    - attribut $`\rightarrow`$ attribut

# 2. La jointure
Deux nuplets qui ont la même valeur pour la partie commune (ici B) peuvent être combinés pour obtenir un nouveau nuplet
$`type(S) = V; type(S') = W`$
$`type(S \bowtie S') = V \cup W`$
$`S \bowtie S' = { t sur V \cup W | \exists v \in et w \in S', v|_{v \cap w'}t|_v = v, t|_w = w}`$

Relativement efficace à évaluer
1. <mark>Trier chaque relation suivant les attributs communs.</mark>
2. <mark>Les fusionner en les parcourant en parallèle.</mark>

Dans le pire des cas, le nombre de nuplets du résultat est le produit des nuplets des relations en input.
La jointure est associative de ce fait :
$`R1 \bowtie (R2 \bowtie R3) = (R1 \bowtie R2) \bowtie R3`$

## 2.1. Cas limite de la jointure : le produit cartésien x
<mark>C'est le cas où il n'y a pas de jointure</mark>, aucune des valeurs de l'attribut où se fait la jointure ne se trouve d dans l'attribut où se fait la jointure de l'autre table et vis-versa.
$`type(Q) \cap tpye(Q') = \oslash `$
Produit cartésien $`Q x Q' = Q \bowtie Q'`$
$`taille(Q \bowtie Q') = taille(Q) * taille(Q')`$

# 3. Les opérations ensemblistes
Q et Q' des requêtes algébriques qui <mark>retournent des ensembles de nuplets du même types.</mark>
Alors on peut calculer leur $`\cap, \cup, -`$
<mark>Seulement si les deux ensembles ont le même type.</mark>

## 3.1. L'union $`S \cup T`$
$`type(S \cup T) = type(S) = type(T)`$

## 3.2. La différence $`S - T`$
$`type(S - T) = type(S) = type(T)

## 3.3. L'insertion c'est une jointure
$`type(S) = type(T)`$
$`S \cap T = S \bowtie T = (T - (T-S))`$

# 4. La composition
**La composition** <mark>permet d'agréger plusieurs requêtes algèbriques pour obtenir un résultat.</mark>  
*Quels sont les pays pour lesquels nous avons des clients qui sont allés en Corse mais qui ne sont jamais allés en côte ?*
```
Client(id, nom, prénom, ville, pays)
Séjour(id, idClient, idLogement, début, fin)
Logement(id, nom, capacité, type, lieu)
```
$`Q_0 = \delta_{id \rightarrow idClient, nom \rightarrow nomClient} (Client)\bowtie (Séjour \bowtie \delta_{id \rightarrow idLogement} (Logement))`$
*Q1 : liste des clients qui ont déjà fait un séjour en Corse et le pays d'où ils viennent.*
$`Q1 = \pi_{pays, idClient}(\sigma_{lieu = "Corse"}(Q0))`$
$`Q2 = \pi_{idClient}(\sigma _{type = "gite"}(Q0))`$
*Q3 : liste des clients qui ne sont jamais allés dans un gîte.*
$` \delta_{id \rightarrow idClient}(Client) - Q2`$
$`\pi_{pays}(A1 \bowtie Q3)`$

# 5. Des opérations redondantes
## 5.1. Les sélections plus compliquées
La sélection de l'algèbre ne permet que des sélections très simples. On peut imaginer des sélections plus compliquées.
$` \sigma_{F}(R)`$ avec $`F : (A = 1 ou A = b) et ((non(B = 2 et C = D)) et A = D)`$
$`\sigma_{F}(R) = { u \in R | u satisfait F}`$
Facile à réaliser :
$`((\sigma_{A = 1}(R) \cup \sigma_{A = B}(R)) - (\sigma_{B = 2}(R) \bowtie \sigma_{C = D}(R))) \bowtie \sigma_{A = D}(R)`$

## 5.2. La division cartésienne
*Quels sont les cliens qui sont allés dans tous les types de logement.*
1. *Qui est allé dans quel type de logement ?* $`H1 = \pi_{idClient, type}(Q0)`$
2. *Quels sont les types de logement ?* $`H2 = \pi_{type}(Logement)`$
3. $`type(H1 \div H2) = type(H1) - type(H2)`$
4. $`H1 \div H2 = {u| \forall \vee in H2, uv in H1}`$

## 5.3. La semi-jointure
$`type(H3 \bowtie H4) = type(H3)`$
$`H3 \ltimes H4 = { u in H3 | u joint avec un nuplet de H4} = \pi_{type(H3)(H3 \bowtie H4)}`$

## 5.4. La $`\theta - jointure`$
<mark>Permet de joindre des attributs différents.</mark>
*Les débuts de séjour en Corse :*
$`Q1 = \sigma_{lieu = "Corse"} Logement`$
$`Q2 = \pi_{idLogement, début}(Séjour)`$
$`\pi_{début}(Q1 \bowtie_{id = idLogement}Q2)`$

# 6. Evaluations et optimisation
Pour chaque opération de l'algèbre, on peu trouver un algorithme (en fait plusieurs) pour l'évaluer.
A partir de là, pour chaque requête, on peut trouver un programme pour l'évaluer (en fait plusieurs).  
Dans un SGBD, le compilateur traduit une requête de l'algèbre dans un programme, l'optimiseur va lui essayer de trouver un système plus efficace. Il faut minimiser dès qu'on peut la quantité de nuplets, ainsi la sélection est à faire en premier.