- [1. Un mauvais schéma](#1-un-mauvais-schéma)
  - [1.1. Anomalies d'insertions](#11-anomalies-dinsertions)
  - [Anomalies de mises à jour](#anomalies-de-mises-à-jour)
  - [Schémas normalisés](#schémas-normalisés)
- [Un bon schéma](#un-bon-schéma)
  - [La dépendance fonctionnelle](#la-dépendance-fonctionnelle)
  - [La clé](#la-clé)
  - [Normalisation](#normalisation)
- [La normalisation relationnelle](#la-normalisation-relationnelle)
  - [Algorithme de normalisation](#algorithme-de-normalisation)
- [Les schéma entités / association](#les-schéma-entités--association)

# 1. Un mauvais schéma
## 1.1. Anomalies d'insertions
Pas d'insdertion, de logement sans connaître au moins une activité, ou bien il faudrait accepter beaucoup de valeurs nulles. De plus, il faudrait à chaque nouvelle activité, il faut répéter les informations du logement. C'est donc une source de redondance.
## Anomalies de mises à jour
La modification d'un logement( capacité) doit être faire autant de fois qu'il y a d'activités, sous peine d'incohérence. Supprimer une activité, risque de supprimer un logement.
Il faudrait alors tester si l'activité supprimée est la dernière du logement.
## Schémas normalisés
- L'ensemble des attributs est décomposé en plusieurs tables.
- Cette répatition se fait sans perte d'information. Les jointures permettent de reconstituer les liens avant décomposition.
- L'information initiale peut alors être présentée sous forme de vue, sans les inconvénients de la matérialisation.

La conception d'une base relationnelle consiste à obtenir un schéma normalisé et sans perte d'information.

# Un bon schéma
## La dépendance fonctionnelle
Il y a dépendance fonctionnelle $`A \rightarrowB`$ quand la connaissance de la valeur de A implique la connaissance de la valeur de B.

```math
code \rightarrow nom,adress
adresse \rightarrow code,nom
code, activité \rightarrow description
```

Les dépendances fonctionnelle nous permettent de raisonner sur le contenu d'une base et d'analyser sa qualité.

Ne pas respecter la dépendance fonctionnelle dans une base de données est signe d'anomalies.
Deux nuplets ayant les mêmes code doivent être identiques.

## La clé
Une **clé** d'une relation $`R`$ est un sous-ensemble minimal $`C`$ des attributs tel que tout attribut de $`R`$ dépend fonctionnelement de $`C`$.
- (code,activité) est une clé : par trnasivitivé, tous les autres attributs en dépendent.
- code n'est pas une clé, activité et description n'en dépendant pas.
- (code, activité, adresse) n'est pas une clé car non minimal.

## Normalisation
Un schéma de relation $`R`$ est uen troisième forme normale quand tout attribut non-clé dépend directement d'une clé.
Il est possible d'émettre l'approximation suivante : dans toute dépendance fonctionnel $`S \rightarrow A`$ sur les attributs de $`R, S` est une clé.
Le schéma précédent n'est pas normalisé à cause des dépendances fonctionnelles : $`code \rightarrow nom, adresse et adresse \rightarrow code, nom`$.  
Une relation qui n'est pas en troisième forme normale parle de deux type d'objet différents, d'où des redondances et des incohérences.

Le schéma normalisé est :
- Logement (code, nom, adresse) où code et adresse sont des clés.
- Activité (code, activité, description) où

L'unicité des valeurs de clé garantit le respect des dépendances fonctionnelles.

- Toute relation a une clé.
- Tous les attributs dépendent directement de la clé (forme normale).
- Une valeur de clé doit apparaître une seule fois.
- La décomposition en plusieurs relations est compensée par la possibilité d'effectuer des jointures.

# La normalisation relationnelle
Il faut partir d'un schéma contenant tous les attributs connus : `(code, nom, adresse, activité, description)`.  
Puis il faut identifier les dépendances fonctionnelles :
- $`code \rightarrow nom, adresse`$
- $`adresse \rightarrow code, nom`$
- $`code, activité \rightarrow description`$

La décomposition est guidée sur les dépendances fonctionnelles. Les deux premiers définissent une relation : $`Logement (code, nom, adresse)`$. Il y a ainsi, deux clés candidates, on en choisit une comme clé primaire.  
Le second défifnit, également, une relation : $`Activité (code, activité, description)`$. La décomposition en troisième forme normale permet une décomposition sans perte d'information. Il faudra parfois rajouter une relation liant les clés de deux autres relations pour ne pas perdre d'information, l'information est recouvrée par jointure.

## Algorithme de normalisation
Pour obtenir un schéma normalisé il faut, partir d'un schéma de relation $`R`$ global et d'un ensemble de dépendances fonctionnelles minimales et directes. Déterminer alors les clés de R :
- Pour chaque $`DF X \rightarrow A_1 ..., A_n, une relation (X, A_1 ..., A_n) de clé X est créée.
- Union des relations de même clé et les relations de même schéma.
- Pour chaque clé $`C`$ non représentée dans une des relations précédentes, on crée une relation ($`C`$).

# Les schéma entités / association
