- [1. Requêtes mono-table](#1-requêtes-mono-table)
  - [1.1. Forme de base d'une requête SQL](#11-forme-de-base-dune-requête-sql)
  - [1.2. D'où vient SQL ?](#12-doù-vient-sql-)
  - [1.3. Clause `WHERE` =, <, >, != opérateur $`\sigma`$ de l'algèbre](#13-clause-where-----opérateur-sigma-de-lalgèbre)
  - [1.4. Clause `SELECT` opérateurs $`\pi`$ et $`\rho`$](#14-clause-select-opérateurs-pi-et-rho)
  - [1.5. Gestion des doublons](#15-gestion-des-doublons)
- [2. Jointures](#2-jointures)
  - [2.1. Jointure naturelle `NATURAL JOIN`](#21-jointure-naturelle-natural-join)
  - [2.2. Theta jointure](#22-theta-jointure)
- [3. Requêtes imbriquées](#3-requêtes-imbriquées)
  - [3.1. Quand utiliser l'imbrication ?](#31-quand-utiliser-limbrication-)
- [4. La négation](#4-la-négation)
  - [4.1. `EXISTS`](#41-exists)
  - [4.2. `NOT iN`](#42-not-in)
  - [4.3. La quantification universelle](#43-la-quantification-universelle)
- [5. Les agrégats](#5-les-agrégats)
  - [5.1. `GROUP BY`](#51-group-by)
  - [5.2. `HAVING`](#52-having)
- [6. Les vues](#6-les-vues)
  - [6.1. Resctrictions](#61-resctrictions)
- [7. Valeurs nulles](#7-valeurs-nulles)
  - [7.1. `IS NULL`](#71-is-null)
- [8. Jointure externe](#8-jointure-externe)
  - [8.1. `[LEFT / RIGHT] OUTER JOIN`](#81-left--right-outer-join)
  - [8.2. Jointure externe](#82-jointure-externe)
- [9. `ORDER BY`](#9-order-by)

# 1. Requêtes mono-table
## 1.1. Forme de base d'une requête SQL
La forme de base est celle d'un "bloc" constitué de trois clauses.

```sql
SELECT expression
FROM Une_table
[WHERE conditions];
```
- `FROM` : <mark>l'espace de recherche</mark>, c'est toujours une table.
- `WHERE` : <mark>conditions imposées</mark> aux nuplets du `FROM`.
- `SELECT` : <mark>construit un nuplet-résultat</mark> à partir de chaque nuplet du `FROM` satisfaisant le `WHERE`.

```sql
SELECT prenom, nom
FROM Voyageur
[WHERE conditions];
```

$`\pi_{prénom, nom}(\sigma_{idVoyageur = 10}(Voyageur))`$
Done une table avec un nuplet-résultat.
- <mark>L'espace de recherche</mark> est la table `Voyageur`.
- <mark>La condition</mark> est une égalité sur un attribut (`idVoyageur = 10`).
- On construit le nuplet-résultat à partir d'un nuplet-voyageur.

## 1.2. D'où vient SQL ?
Il a pour fondement deux langages complémentaires.
- **L'algèbre relationnelle** est un <mark> ensemble d'opérations applicables à des tables</mark>.
- **La logique formelle** donne à SQL <mark>son aspect déclaratif</mark> (ce que l'on veut et non ce que l'on veut calculer).

## 1.3. Clause `WHERE` =, <, >, != opérateur $`\sigma`$ de l'algèbre
- Comparaison entre <mark>un attribut et une constante</mark>.
- Comparaison entre <mark>un attribut et un autre attribut</mark>.

Le `WHERE` est <mark>une formule propositionnelle</mark> (avec `AND`, `OR`, `NOT` et le parenthésage) combinant ces conditions.

```sql
SELECT *
FROM Logement
WHERE lieu = 'Corse' OR (capacité > 50 AND TYPE = 'Hotel');
```
## 1.4. Clause `SELECT` opérateurs $`\pi`$ et $`\rho`$
La projection correspond à `SELECT` $`A_1, ..., A_n`$.
Le renommage (en algèbre) s'exprime avec `AS`.
```sql
SELECT idVoyageur AS "id", prenom AS "p", nom AS "n" 
FROM Voyageur
WHERE idVoyageur < 30;
```

## 1.5. Gestion des doublons
Pour éviter le tri, <mark>SQL n'élime pas les doublons</mark>.
On peut les éliminer avec `DISTINCT`, par ailleurs `DISTINCT` <mark> ordonne les valeurs</mark>.
```sql
SELECT DISTINCT TYPE
FROM Logement;
```
# 2. Jointures
**Une jointure**, c'est <mark>un produit cartésien composé avec une sélection. </mark>
```sql
SELECT expressions
FROM Table1 CROSS JOIN Table2
[WHERE condition_jointure];
```
Ici, l'espace de recherche (`FROM`) est maintenant <mark>la table résultat de $`Table1 * Table2`$</mark>.
<mark>Sans clause `WHERE`, on effectue un produit cartésien, où toutes les associations possibles sont effectuées.</mark>
```sql
SELECT *
FROM Logement CROSS JOIN Activite
```
```sql
SELECT V.nom AS nomVoyageur, L.nom AS nomLogement
FROM Logement AS L, Sejour AS S, Voyageur as V
WHERE code = S.codeLogement
AND S.idVoyageur = V.idVoyageur;
```
Dans la clause `FROM`, <mark>on utilise le plus souvent la `,` à la place de `CROSS JOIN`.</mark>  
**Le renommage** `AS` sert, notamment, <mark>à gérer les ambiguités soulevées par les noms d'attributs.</mark>

## 2.1. Jointure naturelle `NATURAL JOIN`
La `NATURAL JOIN` <mark> va faire la jointure sur les colonnes en commun</mark>, pas besoin de faire de renommage.
```sql
SELECT *
FROM Sejour NATURAL JOIN Voyageur
```

## 2.2. Theta jointure
<mark>Les attributs de jointure n'ont pas le même nom</mark>, c'est juste une syntaxe différente.
```sql
SELECT *
FROM Logement JOIN Activite ON (code = codeLogement)
```

# 3. Requêtes imbriquées
```sql
SELECT DISTINCT nom
FROM Logement, Activite
WHERE code = codeLogement
AND codeActivite = "Ski"
```
*Les logements où l'on peut faire du ski.*

```sql
SELECT nom
FROM Logement
WHERE EXISTS (
    SELECT ''
    FROM Activite
    WHERE code = codeLogement
    AND codeActivite = "Ski")
)
```
*Les logements où on peut faire du ski.*
Ici, la clause `SELECT` de la requête imbriquée ne sert à rien.

```sql
SELECT nom
FROM Logement
WHERE code IN (
    SELECT codeLogement
    FROM Activite
    WHERE codeActivite = "Ski"
)
```
Il y a correspondance entre les attributs code du bloc principal et codeLogement de la requête imbriquée.
**Requête corrélées** à cause du lien établi entre les deux blocs.

## 3.1. Quand utiliser l'imbrication ?
Condition : <mark>c'est une jointure</mark>, mais on construit le résultat avec les nuplets d'une seul relation. Dans l'algèbre, c'est une **semi-jointure**. L'imbrication n'apporte pas d'expressivité supplémentaire. Son utilisation dépend de :
- La lisibilité du code, au-delà d'un niveau d'imbrication, l'interprétation devient laborieuse.
- On ne peut pas construire le nuplet-résultat avec les nuplets des sous-requpetes.

```sql
SELECT prenom, nom
FROM Voyageur AS V
WHERE EXISTS (
    SELECT ''
    FROM Sejour AS S
    WHERE V.idVoyageur = S.idVoyageur
    AND EXISTS (
        SELECT ''
        FROM Logement
        WHERE codeLogement = code
        AND lieu = "Corse"
    )
)
```
Equivaut à :
```sql
SELECT DISTINCT V.prenom, V.nom
FROM Voyageur AS V, Sejour AS S, Logement AS L
WHERE V.idVoyageur = S.idVoyageur
AND codeLogement = code
AND lieu = "Corse"
```
Tant qu'on n'exprime pas de négation, l'imbrication n'apporte que des matières alternatives d'exprimer une même requête.

# 4. La négation
*Les logements qui ne proposent pas de Ski.*
Cette requête n'est pas la solution :
```sql
SELECT DISTINCT nom
FROM Logement, Activite
WHERE code = codeLogement
AND codeActivite != "Ski"
```
Cette requête donne les logements où il existe une activité autre que "Ski".
On veut ici exprimer une condition (négative) sur un ensemble "il ne doit pas exsiter d'activité Ski parmi celles du logement".

## 4.1. `EXISTS`
"Il ne doit pas exister d'actvité Ski parmi celles du logement"
C'est donc une **condition existentielle (négative)**.
```sql
SELECT nom
FROM Logement
WHERE NOT EXISTS (
    SELECT ''
    FROM Activite
    WHERE code = codeLogement
    AND codeActivite = "Ski"
)
```

## 4.2. `NOT iN`
```sql
SELECT nom
FROM Logement
WHERE code NOT IN (
    SELECT codeLogement
    FROM Acitivté
    WHERE codeActivite = "Ski"
)
```

## 4.3. La quantification universelle
"Je veux les voyageurs qui sont allés dans tous les logements"
Si je suis allé dans tous les logements, il n'existe pas de logements où je ne suis pas allé.  
La quantification universelle s'exprime avec **une double quantification existentielle et la négation**... c'est lourd.
Double imbrication, double quantification et double négation.
```sql
SELECT DISTINCT v.prenom, v.nom
FROM Voyageur AS v
WHERE NOT EXISTS (
    SELECT ''
    FROM Logement as l
    WHERE NOT EXISTS (
        SELECT ''
        FROM Sejour AS S
        WHERE l.code = s.codeLogement
        AND v.idVoyageur = s.idVoyageur
    )
)
```

# 5. Les agrégats
On définit **des groupes de lignes** <mark>partageant une ou plusieurs valeurs.</mark>
On ramène chaque groupe à une seule valeur en appliquant **une fonction d'agrégation**.
Cas le plus simple : groupe = résultat d'une requête classique.
```sql
SELECT COUNT(*) AS nombre, SUM(capacite) AS totalCapacite
FROM Logement
WHERE TYPE = "Hotel"
```

## 5.1. `GROUP BY`
La clause `GROUP BY` $`attribut_1, ..., attribut_n`$ <mark>partitione le résulat d'un `SELECT FROM WHERE`</mar> en fonction des $`attribut_1, ..., attribut_n`$.
Chaque groupe contient les lignes qui partagent les mêmes valeurs pour $`attribut_1, ..., attribut_n`$.
```sql
SELECT TYPE, COUNT(*) AS Nombre, SUM(capacite) AS totalCapacite
FROM Logement
GROUP BY TYPE
```

D'abord, on groupe (on obtient une structure intermédiaire qui n'est pas une table en première forme normale [atomicité des valeurs]), puis on agrège (on ramène chaque groupe à une valeur, cette fois, c'est une table en première forme normale).

## 5.2. `HAVING`
<mark>Filtrage dans un deuxième temps</mark>, après calcul des agrégats, portant sur le résultat de la fonction d'agrégation.
Bien distinguer de la clause `WHERE` qui s'applique aux nuplets.
```sql
SELECT TYPE, COUNT(*) AS nombre, SUM(capacite) as totalCapacite
FROM Logement
GROUP BY TYPE
HAVING COUNT(*) > 1
```

# 6. Les vues
<mark>Toutes requêtes produit un relation</mark>
Nommer cette requête c'est nommer la relation résultat.
**Une vue**, <mark>c'est une requête nommée</mark>, on peut alors l'interroger comme n'importe quelle table.  
On peut la considérer comme <mark>une relation calculée et l'interroger</mark> comme les autres, très utile pour:
- <mark>Filtrer une partie de la base.
- <mark>Simplifier et contrôler les accès

```sql
CREATE VIEW LogementCorse AS
SELECT *
FROM Logement
WHERE lieu = "Corse";
```
```sql
SELECT *
FROM LogementCorse;
```
Le résultat de la requête est <mark>réévaluée à chaque fois</mark> que l'on accède à la vue.
Il est également possible de construire des vues à partir d'autres vues.

```sql
CREATE VIEW VoyageurEnCorse AS
SELECT V.nom AS nomVoyageur, L.nom AS nomLogement
FROM LogementCorse AS L, Séjour AS S, Voyageur V
WHERE code = S.codeLogement
```

## 6.1. Resctrictions
On peut insérer dans une vue comme dans une table, mais c'est <mark>à éviter</mark>, car:
- La vue <mark>DOIT être basée sur une seule table</mark>.
- Toute colonne non référencée dans la vue <mark>DOIT pouvoir être mise à `NULL` ou disposer d'une valeur par défaut.
- On ne peut pas mettre à jour un attribut, résultant d'un calcul ou d'une opération.

# 7. Valeurs nulles
Une valeur nulle est <mark>une valeur manquante</mark>. Il ne faut pas confondre avec une chaîne de caractères "nulle" ou "".
<mark>La présence de valeurs nulles fausse le résultat attendu des requêtes.</mark>
<mark>On ne sait ni comparer une valeur nulle, ni appliquer une fonction</mark>.
**Une comparaison avec `NULL`** <mark>ne donne ni vrai, ni faux</mark>, mais une troisième valeur de vérité, **`unknwon`**.

```sql
SELECT *
FROM Logement
WHERE lieu = "";
```

```sql
SELECT *
FROM Logement
WHERE lieu != "";
```
Aucune de ces 2 requêtes n'évaluera le caractère `NULL`.
TOut calcul appliqué à une valeur `NULL`, renvoie `NULL`.

```SQL
SELECT nom, capacité * 2 AS "doubleCapacité"
FROM Logement;
```

$`2 * null = null`$

## 7.1. `IS NULL`
Il faut tester explicitement l'absence de valeur `IS NULL`.

```sql
SELECT *
FROM Logement
WHERE capacité IS NULL;
```
<mark>Le test capacité `= NULL` ne fonctionne pas!</mark>.
Il faut éviter autant que possible les valeurs nulles en les interdisant (dans le schéma).

# 8. Jointure externe
On veut la liste des logements avec leur activités.
```sql
SELECT nom, codeActivité
FROM Logemet, Activité
WHERE code = codeLogement;
```

## 8.1. `[LEFT / RIGHT] OUTER JOIN`
- Renvoie tous les nuplets de la table directrice (à gauche ou à droite).
- <mark>Associe à chaque nuplet un nuplet de la table de droit si un tel nuplet existe</mark>.
- Sinon, <mark>les attributs de la table de droit sont à `NULL`.</mark>


```sql
SELECT nom, codeActivité
FROM Logement LEFT OUTER JOIN Activité ON (code = codeLogement);
```

Ainsi, on obtiendra des valeurs nulles pour certains Logement qui n'ont pas d'activité en faisant une jointure sur code = codeLogement.

## 8.2. Jointure externe
On veut la liste des logements avec leurs activités.

```sql
SELECT nom, codeActivité
FROM Logement, Activité
WHERE code = codeLogement;
```

# 9. `ORDER BY`
On peut demander explicitement le tri du résultat sur une ou plusieurs expressions avec la clause `ORDER BY`.

```sql
SELECT *
FROM Logement
ORDER BY lieu, capacité;
```

On peut ajouter une clause sur l'ordre du tri (ascending ou descending).

```sql
SELECT *
FROM Logement
ORDER BY Lieu DESC, capacité DESC;
```
