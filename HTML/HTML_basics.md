- [1. Syntaxe](#1-syntaxe)
- [2. Structure](#2-structure)
- [3. `<head></head>`](#3-headhead)
  - [3.1. `<meta>`](#31-meta)
  - [3.2. `<link>`](#32-link)
  - [3.3. `<script></script>`](#33-scriptscript)
  - [3.4. `<style></style>`](#34-stylestyle)
- [4. `<body></body>`](#4-bodybody)
  - [Les titres `<h1></h1>](#les-titres-h1h1)
  - [Les sauts de ligne `<br>`](#les-sauts-de-ligne-br)
  - [Mettre du contenu en avant](#mettre-du-contenu-en-avant)
  - [Les listes](#les-listes)
    - [La liste non ordonnée `<ul></ul>`](#la-liste-non-ordonnée-ulul)
    - [La liste ordonnée `<ol></ol>`](#la-liste-ordonnée-olol)
  - [Les caractères reservés](#les-caractères-reservés)
  - [Séparateur de thématique `<hr>`](#séparateur-de-thématique-hr)
  - [Le texte pré-formaté](#le-texte-pré-formaté)
  - [4.1. hypertext link `<a></a>`](#41-hypertext-link-aa)
    - [4.1.1. Lien interne](#411-lien-interne)
  - [4.2. les tableaux  `<table></table>`](#42-les-tableaux--tabletable)
    - [4.2.1. `<thead></thead>` Haut de tableau](#421-theadthead-haut-de-tableau)
    - [4.2.2. `<tbody></tobdy>` Corps du tableau](#422-tbodytobdy-corps-du-tableau)
    - [4.2.3. `<tfoot></tfoot>` Bas du tableau](#423-tfoottfoot-bas-du-tableau)
    - [4.2.4. `<th></th>` Nom de colonnes](#424-thth-nom-de-colonnes)
  - [4.3. `<iframe></iframe>` Permet de charger une autre page à l'intérieur](#43-iframeiframe-permet-de-charger-une-autre-page-à-lintérieur)
  - [`<div></div>` Diviser le contenu (block)](#divdiv-diviser-le-contenu-block)
  - [`<span></span>` Diviser le contenu (inline)](#spanspan-diviser-le-contenu-inline)
  - [UX structure](#ux-structure)
    - [nav](#nav)
    - [header](#header)
    - [aside](#aside)
    - [footer](#footer)
    - [article](#article)
    - [section](#section)
  - [`<form></form>` Les formulaires](#formform-les-formulaires)
# 1. Syntaxe
Balise ouvrante + contenu + balise fermante (balise = tag en anglais).

```html
<p>contenu</p>
```
De plus, la balise ouvrante peut avoir des attributs.

```html
<p class = "attribut">contenu</p>
```

Les éléments HTML peuvent être imbriqués entre eux,  l'ordre de fermeture des balises est une pile (LIFO : Last In First Out).

```html
<p>Ex<strong>em</strong>ple</p>
```

Elements vides : balise ouvrante seule 

```html
<img src  = "img.png" alt = "text alternatif">
```

# 2. Structure
Pour améliorer la visualisation du code, il est conseillé d'indenter les éléments imbriqués.
Structure de base d'une page html :

```html
<!DOCTYPE html>
<html lang = "fr">
    <head>
        <meta charset = "utf-8">
        <title>Ma page</title>
    </head>
    <body>
        <p>contenu</p>
    </body>
</html>
```
DOCTYPE permet de spécifier les régles appliqués au document HTML
html : élément racine entourant toute la page
head : toutes les informations qui ont attrait à la page web, essentiellement utilisé par les moteurs de recherche, ce qui n'est pas visible par l'utilisateur.
meta : donne des informations sur la page, comme le type d'encodage avec l'attribut `charset`.
title : titre de la page
body : contenu de la page, ce qui est visibile par l'utilisateur.

# 3. `<head></head>`
## 3.1. `<meta>`
Permet de définir les métadonnées du document.
```html
<meta charset = "utf-8">
<meta name = "description" content = "description de mon site"> 
```
Le contenu de la description utilisé ici, est utilisé lors de l'affichage des sites web dans une requête effectué sur un moteur de recherche.
```html
<meta name = "keywords" content = "Biology, embryology, KNN">
```
Permet de définir des mots cléfs illustrant un site.
## 3.2. `<link>`
Permet de faire le lien vers une ressource externe à la page web : page css ou image (par exemple dans la favicon).
```html
<link rel = "stylesheet" href = "css/page.css">
<link rel = "icon" type = "image/png" href = "logo.png">
```

## 3.3. `<script></script>`
Permet de mettre directement du code javascript.

## 3.4. `<style></style>`
Permet de mettre directement du code css.

# 4. `<body></body>`
## Les titres `<h1></h1>
Il existe 6 niveaux de titres :
```html
<h1>Titre niveau 1</h1>
<h6>Titre niveau 6</h6>```ħtml
<ul>
  <li>Une élément de la liste.</li>
  <li>Un autre élément de la liste.</li>
</ul>
```

## Les sauts de ligne `<br>`
Les sauts de ligne ne sont interprétés par défaut.
```html
<p>Première ligne
Toujours la première ligne <br>
Deuxième ligne
</p>
```

## Mettre du contenu en avant
Il existe deux moyens de mettre du contenus en avance :
```html
<p><strong>Texte important</strong> <br>
<em>texte accentué</em>
</p>
```

- `<em>` : peut être utilisé lorsque l'on site quelqu'un ou qu'on écrit du texte dans un autre langage.
- `<strong>` : peut être utilisé pour mettre en avant certains textes.

## Les listes
### La liste non ordonnée `<ul></ul>`
```html
<ul>
  <li>Une élément de la liste.</li>
  <li>Un autre élément de la liste.</li>
</ul>
```

### La liste ordonnée `<ol></ol>`
```html
<ol>
  <li>Une élément de la liste.</li>
  <li>Un autre élément de la liste.</li>
</ol>
```

## Les caractères reservés
Pour éviter que HTML n'interpréte certains caractères, on peut utiliser les caractères suivant :
|Caractère|Entité|
|: --- :|: --- :|
|&|`&amp;`|
|<|`&lt;`|
|>|`&gt;`|
|"|`&quot;`

## Séparateur de thématique `<hr>`
Ce séparateur de thématique va être interprété comme une ligne horizontale.
```html
<p>blablabla</p>
<hr>
<p>bliblibli</p>
```


## Le texte pré-formaté
```html
<pre>
 1+1 
          2+2+2+2
</pre>
```


## 4.1. hypertext link `<a></a>`
Permet de faire des liens vers des pages d'un dossier ou encore vers une adresse.

```html
<a href = "menu/index.html">Accueil (chemin relatif)</a>
<a href = "/index.html">Accueil (chemin absolu)</a>
<a href = "../index.html">Accueil (chemin relatif)</a>
<a href = "https://www.google.com">Google (url)</a>
```
- https:// protocole.
- www.google.com nom du domaine.

Il est possible d'ouvrir les pages webs dans un nouvel onglet :
 
```html
<a href = "menu/index.html" target = "_blank">Accueil dans un nouvel ongle</a>
```

### 4.1.1. Lien interne
**Les ancres / liens internes** vont permettrent d'accéder spécifiquement à des éléments de la page web grâce à des id. <mark>L'id doit être unique</mark>.
```html
<a href = "#cuisine">Cuisine (lien vers l'id du titre h2 Cuisine)</a>
<h2 id = "cuisine">Cuisine</a>

## Les images `<img>`
Pour afficher une image, il faut :
- Spécifier un chemin avec l'attribut `src = ""`.
- Spécifier une alternative si l'image ne se charge pas, pour les moteurs de recherches ou pour les personnes déficientes visuellement, à l'aide de l'attribut `alt = ""`.

```html
<img src = "images/photo.png" alt = "photo">
<img src = "images/photo.png" alt = "photo">
```

Redimensionner les images non vectorielles et les stocker dans un dossier, car si on redimensionne à l'aide d'HTML ou CSS, l'image est chargée dans sa résolution initiale puis redimensionnée.

## 4.2. les tableaux  `<table></table>`
Permet d'afficher les données sous forme de tableau.
- `<tr></tr> : représente une nouvelle ligne dans le tableau.
- `<td></td> : représente une nouvelle cellule dans le tableau.

```html
<table>
  <tr>
    <td>1ère cellule de la 1ère ligne</td>
    <td>2ème cellule de la 1ère ligne</td>
  </tr>
  <tr>
    <td>1ère cellule de la 2ème ligne</td>
    <td>2ème cellule de la 2ème ligne</td>
  </tr>
</table>
```

<mark>Les lignes doivent avoir le même nombre de cellules.</mark> Pour palier à ce problème, il est possible d'utiliser l'attribut `<td colspan = "2">Cellule de 2 colonnes</td>`. On peut exactement faire la même chose avec les lignes, `<td rowspan = "2">Cellule à 2 lignes</td>`.


```html
<table>
  <tr>
    <td colspan = "2">1ère cellule de la 1ère ligne de taille = 2 colonnes</td>
  </tr>
  <tr>
    <td>1ère cellule de la 2ème ligne</td>
    <td>2ème cellule de la 2ème ligne</td>
  </tr>
</table>
```

```html
<table>
  <tr>
    <td rowspan = "2">1ère cellule de la première colone avec s'étendant sur 2 lignes</td>
    <td>2ème cellule de la 2ème colonne</td>
  </tr>
  <tr>
    <td>2ème cellule de la 2ème ligne</td>
  </tr>
</table>
```


### 4.2.1. `<thead></thead>` Haut de tableau
```html
<table>
  <thead>
    <tr>
      <td colspan = "2">1ère cellule de la 1ère ligne de taille = 2 colonnes</td>
    </tr>
  </thead>
    <tr>
      <td>1ère cellule de la 2ème ligne</td>
      <td>2ème cellule de la 2ème ligne</td>
    </tr>
</table>
```

### 4.2.2. `<tbody></tobdy>` Corps du tableau
```html
<table>
  <thead>
    <tr>
      <td colspan = "2">1ère cellule de la 1ère ligne de taille = 2 colonnes</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1ère cellule de la 2ème ligne</td>
      <td>2ème cellule de la 2ème ligne</td>
    </tr>
  </tbody>
</table>
```

### 4.2.3. `<tfoot></tfoot>` Bas du tableau
```html
<table>
  <thead>
    <tr>
      <td colspan = "2">1ère cellule de la 1ère ligne de taille = 2 colonnes</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1ère cellule de la 2ème ligne</td>
      <td>2ème cellule de la 2ème ligne</td>
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <td colspan = "2">Bas de tableau</td>
    </tr>
  </tfoot>
</table>
```

### 4.2.4. `<th></th>` Nom de colonnes
```html
<table>
  <thead>
    <tr>
      <td colspan = "2">1ère cellule de la 1ère ligne de taille = 2 colonnes</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Entête 1</th>
      <th>Entête 2</th>
    </tr>
    <tr>
      <td>1ère cellule de la 2ème ligne</td>
      <td>2ème cellule de la 2ème ligne</td>
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <td colspan = "2">Bas de tableau</td>
    </tr>
  </tfoot>
</table>
```

##  4.3. `<iframe></iframe>` Permet de charger une autre page à l'intérieur
On peut par mettre des cartes, des vidéos youtubes etc.
On peut désactiver le fait qu'une page soit intégrée ou non par un iframe dans une autre page.

```html
<iframe src = "index.html" height = "600" width = "300"></iframe>
```

Le contexte de navigation qui contient le contenu intégré est appelé « contexte de navigation parent ». Le contexte de navigation le plus élevé (qui n'a pas de contexte parent) correspond généralement à la fenêtre du navigateur.

## `<div></div>` Diviser le contenu (block)
Permet d'entourer différent éléments (block) et de les regrouper. Cela peut permettre de donner des indications de langue par exemple, mais aussi d'attribuer des id spéciaux à plusieurs éléments.

## `<span></span>` Diviser le contenu (inline)
Permet d'entourer des éléments de texte et de leur donner un id ou des attributs à l'instar des div.


## UX structure
Structurer sa page à l'aide des balises suivantes est utile pour les moteurs de recherches et donne plus de sens que plusieurs éléments `<div></div>`.

### nav
### header
### aside
### footer
### article
### section

## `<form></form>` Les formulaires
L'attribut action, permet de rediriger l'utilisateur quand il appuiera sur le bouton submit, par exemple
`<fieldset></fieldset>` permet de  regrouper une partie d'un formulaire HTML et de leur associer une légende.  
`<label></label>` permet d'attacher un label à un input. L'attribut for permet de spécifier l'id d'un input, ainsi en cliquant sur le label ça clique automatiquement sur la zone de saisie.  
`<input>` permet de saisir une valeur de l'utilisateur. L'attribut type restreint les types de valeurs de l'utilisateur :
- text : du texte.
- date : une date.
- password : du texte mais caché à la sasie .
- number : que des nombres.
- checkbox : saisir plusieurs champs.
- radio : saisir un champ seulement.
- file : upload un file.
- submit : soumettre les informations du formulaire.

Au lieu d'utiliser `<input type="submit">`, il est possible d'utiliser `<button></button>.

`<textare></textarea>` permet d'écrire du texte en conservant les sauts de ligne.
`<select></select>` permet à l'utilisateur de choisir une ou plusieurs (si l'attribut multipel) valeurs parmi une liste déroulante.



```html
<form action = "" >
  <fieldset>
    <legend>Nous contacter</legend>
    <div>
      <label for = "emailInput">Email
        <input type = "text" name = "email" id = "emailInput" placeholder = "valeur_par@defaut.fr" required> 
      </label>
    </div>

    <div>
      <label for = "password">MDP
        <input type = "password" name = "mdp" id = "password">
      </label>
    </div>

    <div>
      <label for = "categories">Catégorie
        <input type = "checkbox" name = "cat" id = "categories">
      </label>
    </div>

    <div>
      <label>Sexe</label>
      <div>
        <input type = "radio" name = "sexe" id = "masculin" checked>
        <label for = "masculin">Masculin</label>
      </div>

      <div>
        <input type = "radio" name = "sexe" id = "feminin">
        <label for="feminin">Femin</label>
      </div>
    </div>

    <div>
      <label for="sex">Sex</label>
      <select name="sex" id="sex">
        <option>M</option>
        <option>F</option>
      </select>
    </div>

    <label for="message">Votre message</label>
    <textarea name="message" id="message" cols="30" rows="10" placeholder="Votre message">

    <div>
      <label for="fileInput">Pièce Jointe<label>
        <input type="file" name="file" id="fileInput">
      </label>
    </div>

    <div>
      <label><input type="checkbox" name="cgu" checked> J'accepte les conditions d'utilisations</label>
    </div>

    <div>
      <label for = "animal">Animal préféré</label>
      <select name="animal" id="animal">
        <optgroup label="Mammifère">
          <option>Cheval</option>
          <option>Chat</option>
        </optgroup>

        <optgroup label="Oiseau">
          <option>Poule</option>
          <option>Dindon</option>
        </optgroup>
      </select>
    </div>
    
    <input type="submit" value="soumettre">
    <input type="reset" value="reset">
  </fieldset>
</form>