#!/usr/bin/env python4

# Exercice from :
# https://github.com/suraggupta/coursera-machine-learning-solutions-python/blob/master/Exercise1/exercise1.ipynb
# I just commented some functions that I didn't know and try to shorten the code for my own purpose

import copy
# used for manipulating directory paths
import os

# Scientific and vector computation for python
import numpy as np

# Plotting library
from matplotlib import pyplot

# Linear regression with two variables

def plotData(X, y):
    """
    Plots the data points X and y into a new figure. Plots the data
    points and gives the figure aXes labels of population and profit.
    Parameters
    ----------
    X : array_like
        Data point values for X-aXis.
    y : array_like
        Data point values for y-aXis. Note X and y should have the same size.
    Instructions
    ------------
    Plot the training data into a figure using the "figure" and "plot"
    functions. Set the aXes labels using the "Xlabel" and "ylabel" functions.
    Assume the population and revenue data have been passed in as the X
    and y arguments of this function.
    Hint
    ----
    You can use the 'ro' option with plot to have the markers
    appear as red circles. Furthermore, you can make the markers larger by
    using plot(..., 'ro', ms=10), where `ms` refers to marker size. You
    can also set the marker edge color using the `mec` property.
    """
    # pyplot.figure() :
    # - Obtaining a handle to a figure. In many cases it is useful to have a handle to a figure, i.e. a variable to store the matplotlib.figure.Figure instance in, such that it can be used later on.
    # - Set figure parameters. An option to set some of the parameters for the figure is to supply them as arguments to plt.figure
    # - Create several figures. In order to create several figures in the same script, plt.figure can be used.

    fig = pyplot.figure()  # open a new figure
    # ====================== YOUR CODE HERE =======================
    pyplot.plot(X, y, 'ro', ms =10, mec = 'k')
    pyplot.ylabel('Profit in $10,000')
    pyplot.xlabel('Population of City in 10,000s')
    # =============================================================

def cost_function(X, y, theta):
    """
    Compute cost for linear regression. Computes the cost of using theta as the
    parameter for linear regression to fit the data points in X and y.
    Parameters
    ----------
    X : array_like
        The input dataset of shape (m X n+1), where m is the number of eXamples,
        and n is the number of features. We assume a vector of one's already
        appended to the features so we have n+1 columns.
    y : array_like
        The values of the function at each data point. This is a vector of
        shape (m, ).
    theta : array_like
        The parameters for the regression function. This is a vector of
        shape (n+1, ).
    Returns
    -------
    J : float
        The value of the regression cost function.
    Instructions
    ------------
    Compute the cost of a particular choice of theta.
    You should set J to the cost.
    """
    # samplesNumber = len(predictions) # Number of observations/rows
    # samplesNumber = y.size() # equivalent
    # np.dot() == matriX product
    # np.square() square of elements
    # np.sum() sum of all element in a given matriX, could be based on aXis (0 or 1)
    # np.sum : returns the sum of matrix elements
    # np.square : return a squared matrix
    result = (1/(2 * y.size)) * np.sum(np.square(np.dot(X, theta) - y))
    return result

def gradient_descent(X, y, theta, alpha, num_iters):
    """
    Performs gradient descent to learn `theta`. Updates theta by taking `num_iters`
    gradient steps with learning rate `alpha`.
    Parameters
    ----------
    X : array_like
        The input dataset of shape (m X n+1).
    y : arra_like
        Value at given features. A vector of shape (m, ).
    theta : array_like
        Initial values for the linear regression parameters.
        A vector of shape (n+1, ).
    alpha : float
        The learning rate.
    num_iters : int
        The number of iterations for gradient descent.
    Returns
    -------
    theta : array_like
        The learned linear regression parameters. A vector of shape (n+1, ).
    J_history : list
        A python list for the values of the cost function after each iteration.
    Instructions
    ------------
    Peform a single gradient step on the parameter vector theta.
    While debugging, it can be useful to print out the values of
    the cost function (computeCost) and gradient here.
    """
    # Extracting the X and y values corresponding by rows
    theta = theta.copy
    J_history = []
    for i in range(num_iters):
        # y.shape[0] : The shape attribute for numpy arrays returns the dimensions of the array.
        # np.dot : returns the dot product of vectors a and b. It can handle 2D arrays but considering them as matrix and will perform matrix multiplication. For N dimensions it is a sum product over the last axis of a and the second-to-last of b
        theta = theta - (alpha / y.shape[0]) * (np.dot(X, theta) - y).dot(X)
        J_history.append(cost_function(X, y, theta))
    #J_history = [cost_function(X, y, theta - (alpha / y.size) * (np.dot(X, theta) - y).dot(X)) for i in range(num_iters)]
    return theta, J_history

# Loading data
# data =  np.random.rand(2,100)
# set path work directory
os.chdir("/home/simon/Bureau")
data = np.loadtxt("data.txt", delimiter=',')
X,y = data[:, 0], data[:, 1]
# stack() function is used to join a sequence of same dimension arrays along a new axis. The axis parameter specifies the index of the new axis in the dimensions of the result. For example, if axis=0 it will be the first dimension and if axis=-1 it will be the last dimension.
X = np.stack([np.ones(y.size), X], axis=1)

# initialize fitting parameters with a vector of 2 zeros
theta = np.zeros(2)

# some gradient descent settings
iterations = 1500
alpha = 0.01

theta, J_history = gradient_descent(X, y, theta, alpha, iterations)
# *theta seems to mean all element of a container
print('Theta found by gradient descent: {:.4f}, {:.4f}'.format(*theta))
print('EXpected theta values (approXimately): [-3.6303, 1.1664]')

# plot the linear fit
plotData(X[:, 1], y)
pyplot.plot(X[:, 1], np.dot(X, theta), '-')
pyplot.legend(['Training data', 'Linear regression'])
pyplot.show()


# Plot the cost over 2-dimensional grid of theta0 and theta1 values
# grid over which we will calculate J
theta0_vals = np.linspace(-10, 10, 100)
theta1_vals = np.linspace(-1, 4, 100)

# initialize J_vals to a matrix of 0's
J_vals = np.zeros((theta0_vals.shape[0], theta1_vals.shape[0]))

# Fill out J_vals
for i, theta0 in enumerate(theta0_vals):
    for j, theta1 in enumerate(theta1_vals):
        J_vals[i, j] = computeCost(X, y, [theta0, theta1])

# Because of the way meshgrids work in the surf command, we need to
# transpose J_vals before calling surf, or else the axes will be flipped
# .T : to get the transposed matrix
J_vals = J_vals.T

# surface plot
fig = pyplot.figure(figsize=(12, 5))
ax = fig.add_subplot(121, projection='3d')
ax.plot_surface(theta0_vals, theta1_vals, J_vals, cmap='viridis')
pyplot.xlabel('theta0')
pyplot.ylabel('theta1')
pyplot.title('Surface')

# contour plot
# Plot J_vals as 15 contours spaced logarithmically between 0.01 and 100
ax = pyplot.subplot(122)
pyplot.contour(theta0_vals, theta1_vals, J_vals, linewidths=2, cmap='viridis', levels=np.logspace(-2, 3, 20))
pyplot.xlabel('theta0')
pyplot.ylabel('theta1')
pyplot.plot(theta[0], theta[1], 'ro', ms=10, lw=2)
pyplot.title('Contour, showing minimum')
pass

# Linear regression with multiples variables
# Load data
# .join() : is used to concatenate two string, as paste0() in R
data = np.loadtxt(os.path.join('Data', 'ex1data2.txt'), delimiter=',')
X = data[:, :2]
y = data[:, 2]
m = y.size

# print out some data points
print('{:>8s}{:>8s}{:>10s}'.format('X[:,0]', 'X[:, 1]', 'y'))
print('-'*26)
for i in range(10):
    print('{:8.0f}{:8.0f}{:10.0f}'.format(X[i, 0], X[i, 1], y[i]))


def featureNormalize(X):
    """
    Normalizes the features in X. returns a normalized version of X where
    the mean value of each feature is 0 and the standard deviation
    is 1. This is often a good preprocessing step to do when working with
    learning algorithms.

    Parameters
    ----------
    X : array_like
        The dataset of shape (m x n).

    Returns
    -------
    X_norm : array_like
        The normalized dataset of shape (m x n).

    Instructions
    ------------
    First, for each feature dimension, compute the mean of the feature
    and subtract it from the dataset, storing the mean value in mu.
    Next, compute the  standard deviation of each feature and divide
    each feature by it's standard deviation, storing the standard deviation
    in sigma.

    Note that X is a matrix where each column is a feature and each row is
    an example. You needto perform the normalization separately for each feature.

    Hint
    ----
    You might find the 'np.mean' and 'np.std' functions useful.
    """
    # You need to set these values correctly
    X_norm = X.copy()
    mu = np.zeros(X.shape[1])
    sigma = np.zeros(X.shape[1])

    # =========================== YOUR CODE HERE =====================
    mu = np.mean(X, axis=0)
    sigma = np.std(X, axis=0)
    X_norm = (X - mu) / sigma

    # ================================================================
    return X_norm, mu, sigma

# call featureNormalize on the loaded data
X_norm, mu, sigma = featureNormalize(X)

print('Computed mean:', mu)
print('Computed standard deviation:', sigma)

# Add intercept term to X
# np.concatenate : Join a sequence of arrays along an existing axis
# np.ones() : Python numpy.ones() function returns a new array of given shape and data type, where the element’s value is set to 1.
X = np.concatenate([np.ones((m, 1)), X_norm], axis=1)


def computeCostMulti(X, y, theta):
    """
    Compute cost for linear regression with multiple variables.
    Computes the cost of using theta as the parameter for linear regression to fit the data points in X and y.

    Parameters
    ----------
    X : array_like
        The dataset of shape (m x n+1).

    y : array_like
        A vector of shape (m, ) for the values at a given data point.

    theta : array_like
        The linear regression parameters. A vector of shape (n+1, )

    Returns
    -------
    J : float
        The value of the cost function.

    Instructions
    ------------
    Compute the cost of a particular choice of theta. You should set J to the cost.
    """
    # Initialize some useful values
    m = y.shape[0]  # number of training examples

    # You need to return the following variable correctly
    J = 0

    # ======================= YOUR CODE HERE ===========================
    h = np.dot(X, theta)

    J = (1 / (2 * m)) * np.sum(np.square(np.dot(X, theta) - y))

    # ==================================================================
    return J


def gradientDescentMulti(X, y, theta, alpha, num_iters):
    """
    Performs gradient descent to learn theta.
    Updates theta by taking num_iters gradient steps with learning rate alpha.

    Parameters
    ----------
    X : array_like
        The dataset of shape (m x n+1).

    y : array_like
        A vector of shape (m, ) for the values at a given data point.

    theta : array_like
        The linear regression parameters. A vector of shape (n+1, )

    alpha : float
        The learning rate for gradient descent.

    num_iters : int
        The number of iterations to run gradient descent.

    Returns
    -------
    theta : array_like
        The learned linear regression parameters. A vector of shape (n+1, ).

    J_history : list
        A python list for the values of the cost function after each iteration.

    Instructions
    ------------
    Peform a single gradient step on the parameter vector theta.

    While debugging, it can be useful to print out the values of
    the cost function (computeCost) and gradient here.
    """
    # Initialize some useful values
    m = y.shape[0]  # number of training examples

    # make a copy of theta, which will be updated by gradient descent
    theta = theta.copy()

    J_history = []

    for i in range(num_iters):
        # ======================= YOUR CODE HERE ==========================

        theta = theta - (alpha / m) * (np.dot(X, theta) - y).dot(X)
        # =================================================================

        # save the cost J in every iteration
        J_history.append(computeCostMulti(X, y, theta))

    return theta, J_history

"""
Instructions
------------
We have provided you with the following starter code that runs
gradient descent with a particular learning rate (alpha). 

Your task is to first make sure that your functions - `computeCost`
and `gradientDescent` already work with  this starter code and
support multiple variables.

After that, try running gradient descent with different values of
alpha and see which one gives you the best result.

Finally, you should complete the code at the end to predict the price
of a 1650 sq-ft, 3 br house.

Hint
----
At prediction, make sure you do the same feature normalization.
"""
# Choose some alpha value - change this
alpha = 0.1
num_iters = 400

# init theta and run gradient descent
theta = np.zeros(3)
theta, J_history = gradientDescentMulti(X, y, theta, alpha, num_iters)

# Plot the convergence graph
pyplot.plot(np.arange(len(J_history)), J_history, lw=2)
pyplot.xlabel('Number of iterations')
pyplot.ylabel('Cost J')

# Display the gradient descent's result
print('theta computed from gradient descent: {:s}'.format(str(theta)))

# Estimate the price of a 1650 sq-ft, 3 br house
# ======================= YOUR CODE HERE ===========================
# Recall that the first column of X is all-ones.
# Thus, it does not need to be normalized.

X_array = [1, 1650, 3]
X_array[1:3] = (X_array[1:3] - mu) / sigma
price = np.dot(X_array, theta)   # You should change this

# ===================================================================

print('Predicted price of a 1650 sq-ft, 3 br house (using gradient descent): ${:.0f}'.format(price))

# Normal equation
# Using this formula does not require any feature scaling, and you will get an exact solution in one calculation: there
# is no “loop until convergence” like in gradient descent.
# Load data
data = np.loadtxt(os.path.join('Data', 'ex1data2.txt'), delimiter=',')
X = data[:, :2]
y = data[:, 2]
m = y.size
X = np.concatenate([np.ones((m, 1)), X], axis=1)


def normalEqn(X, y):
    """
    Computes the closed-form solution to linear regression using the normal equations.

    Parameters
    ----------
    X : array_like
        The dataset of shape (m x n+1).

    y : array_like
        The value at each data point. A vector of shape (m, ).

    Returns
    -------
    theta : array_like
        Estimated linear regression parameters. A vector of shape (n+1, ).

    Instructions
    ------------
    Complete the code to compute the closed form solution to linear
    regression and put the result in theta.

    Hint
    ----
    Look up the function `np.linalg.pinv` for computing matrix inverse.
    """
    theta = np.zeros(X.shape[1])

    # ===================== YOUR CODE HERE ============================
    # We use numpy. linalg. inv() function to calculate the inverse of a matrix. The inverse of a matrix is such that if
    # it is multiplied by the original matrix, it results in identity matrix.
    theta = np.dot(np.dot(np.linalg.inv(np.dot(X.T, X)), X.T), y)

    # =================================================================
    return theta

# Calculate the parameters from the normal equation
theta = normalEqn(X, y);

# Display normal equation's result
print('Theta computed from the normal equations: {:s}'.format(str(theta)));

# Estimate the price of a 1650 sq-ft, 3 br house
# ====================== YOUR CODE HERE ======================

X_array = [1, 1650, 3]
X_array[1:3] = (X_array[1:3] - mu) / sigma
price = np.dot(X_array, theta) # You should change this

# ============================================================

print('Predicted price of a 1650 sq-ft, 3 br house (using normal equations): ${:.0f}'.format(price))